# -*- coding: utf-8 -*-

# Copyright (C) 2017 Khaled Hosny and Barry Schwartz
#
# This file is part of the Sorts Mill FFCompat.
# 
# Sorts Mill FFCompat is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
# 
# Sorts Mill FFCompat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.

include 'config.pxi'

IF SIZEOF__BOOL == SIZEOF_CHAR:
  ctypedef char _Bool
ELIF SIZEOF__BOOL == SIZEOF_SHORT:
  ctypedef short _Bool
ELSE:
  ctypedef int _Bool
