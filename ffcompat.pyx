# -*- coding: utf-8 -*-

# Copyright (C) 2017 Khaled Hosny and Barry Schwartz
#
# This file is part of the Sorts Mill FFCompat.
# 
# Sorts Mill FFCompat is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
# 
# Sorts Mill FFCompat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.

cimport core
cimport guile
cimport tools

from cpython cimport array
from cpython.list cimport PyList_New
from cpython.list cimport PyList_GET_SIZE
from cpython.list cimport PyList_GET_ITEM
from cpython.list cimport PyList_SET_ITEM
from cpython.ref cimport Py_INCREF
from libc.math cimport HUGE_VAL
from libc.math cimport M_PI
from libc.math cimport fabs
from libc.math cimport cos
from libc.math cimport sin
from libc.stdint cimport uint8_t
from libc.string cimport strcmp
from types cimport _Bool

include 'config.pxi'

import collections
import cython
import numbers
import sys
import warnings

from array import array

this_module = sys.modules[__name__]

#--------------------------------------------------------------------------

def raise_check_precondition ():
  raise ValueError ("Argument precondition error.")

cdef bint check_precondition (bint precondition) except 0:
  if not precondition:
    raise_check_precondition ()
  return 1

cdef bytes to_bytes (x):
  cdef bytes b
  if isinstance (x, bytes):
    b = x
  elif isinstance (x, unicode):
    b = x.encode ()
  else:
    raise TypeError ("Expected bytes or unicode")
  return b

cdef str to_str (x):
  cdef str s
  if isinstance (x, str):
    s = x
  elif isinstance (x, bytes):
    s = x.decode ()
  else:
    raise TypeError ("Expected bytes or string")
  return s

cdef to_str_or_None (const char *x):
  cdef object s
  cdef bytes b
  if x == NULL:
    s = None
  else:
    b = x
    s = to_str (b)
  return s

cdef bint cmp_to_richcmp (int cmp, int operation):
  cdef bint result
  if operation == 0:
    result = (cmp < 0)
  elif operation == 1:
    result = (cmp <= 0)
  elif operation == 2:
    result = (cmp == 0)
  elif operation == 3:
    result = (cmp != 0)
  elif operation == 4:
    result = (cmp > 0)
  else:
    result = (cmp >= 0)
  return result

cdef inline ssize_t fold_index (size_t n, ssize_t i) except -1:
  # Fold a Python-style index into the range 0 <= j < n.
  cdef size_t j
  if 0 <= i and i < n:
    j = i
  elif -n <= i and i < 0:
    j = n + i
  else:
    raise IndexError ()
  return j

cdef tools.ffcontour_add_extrema_operation get_add_extrema_operation (flag) except *:
  cdef bytes bflag
  cdef const char *bflg
  if flag is None:
    op = tools.FFCONTOUR_add_extrema_default
  else:
    bflag = to_bytes (flag)
    bflg = bflag
    if strcmp (bflg, "all") == 0:
      op = tools.FFCONTOUR_add_all_extrema
    elif strcmp (bflg, "between_selected") == 0:
      # NOTE: The old fontforge module does not support
      # "between_selected", presumably because the author didn’t
      # believe it would be useful. But we include it here.
      op = tools.FFCONTOUR_add_extrema_between_selected
    elif strcmp (bflg, "only_good") == 0:
      op = tools.FFCONTOUR_add_extrema_on_longer_splines
    elif strcmp (bflg, "only_good_rm") == 0:
      op = tools.FFCONTOUR_add_extrema_and_merge_on_longer_splines
    else:
      raise ValueError (flag + " is not a valid argument value.")
  return op

cdef int collect_simplification_flags (flags) except *:
  cdef bytes bflag
  cdef const char *bflg
  cdef int operations = 0
  for f in flags:
    bflag = to_bytes (f)
    bflg = bflag
    if strcmp (bflg, "cleanup") == 0:
      operations |= tools.FFCONTOUR_do_cleanup
    elif strcmp (bflg, "do_cleanup") == 0: # SM Tools extension.
      operations |= tools.FFCONTOUR_do_cleanup
    elif strcmp (bflg, "ignoreslopes") == 0:
      operations |= tools.FFCONTOUR_ignore_slopes
    elif strcmp (bflg, "ignore_slopes") == 0: # SM Tools extension.
      operations |= tools.FFCONTOUR_ignore_slopes
    elif strcmp (bflg, "ignoreextrema") == 0:
      operations |= tools.FFCONTOUR_allow_removal_of_extrema
    elif strcmp (bflg, "allow_removal_of_extrema") == 0: # SM Tools extension.
      operations |= tools.FFCONTOUR_allow_removal_of_extrema
    elif strcmp (bflg, "smoothcurves") == 0:
      operations |= tools.FFCONTOUR_smooth_curves
    elif strcmp (bflg, "smooth_curves") == 0: # SM Tools extension.
      operations |= tools.FFCONTOUR_smooth_curves
    elif strcmp (bflg, "choosehv") == 0:
      operations |= tools.FFCONTOUR_choose_hv
    elif strcmp (bflg, "choose_hv") == 0: # SM Tools extension.
      operations |= tools.FFCONTOUR_choose_hv
    elif strcmp (bflg, "forcelines") == 0:
      operations |= tools.FFCONTOUR_force_lines
    elif strcmp (bflg, "force_lines") == 0: # SM Tools extension.
      operations |= tools.FFCONTOUR_force_lines
    elif strcmp (bflg, "nearlyhvlines") == 0:
      operations |= tools.FFCONTOUR_snap_nearly_hv_lines
    elif strcmp (bflg, "snap_nearly_hv_lines") == 0: # SM Tools extension.
      operations |= tools.FFCONTOUR_snap_nearly_hv_lines
    elif strcmp (bflg, "mergelines") == 0:
      operations |= tools.FFCONTOUR_merge_lines
    elif strcmp (bflg, "merge_lines") == 0: # SM Tools extension.
      operations |= tools.FFCONTOUR_merge_lines
    elif strcmp (bflg, "setstarttoextremum") == 0:
      operations |= tools.FFCONTOUR_set_start_to_extremum
    elif strcmp (bflg, "set_start_to_extremum") == 0: # SM Tools extension.
      operations |= tools.FFCONTOUR_set_start_to_extremum
    elif strcmp (bflg, "setstarttoextrema") == 0:
      # This case makes up for a documentation error.
      warnings.warn ('"setstarttoextrema" is deprecated; use "setstarttoextremum" instead.',
                     DeprecationWarning)
      operations |= tools.FFCONTOUR_set_start_to_extremum
    elif strcmp (bflg, "removesingletonpoints") == 0:
      operations |= tools.FFCONTOUR_remove_singleton_points
    elif strcmp (bflg, "remove_singleton_points") == 0: # SM Tools extension.
      operations |= tools.FFCONTOUR_remove_singleton_points
    else:
      raise ValueError ('Invalid simplify-operation flag: "' + to_str (bflag) + '"')
  return operations

cdef list make_spline_as_list (tools.ffspline_t u):
  cdef size_t n = tools.ffspline_length (u)
  cdef size_t i
  return [tools.ffspline_ref (u, i) for i in range (n)]

cdef list make_list_of_splines (tools.ffspline_ivect_t v):
  cdef size_t n = tools.ffspline_ivect_length (v)
  cdef size_t i
  return [make_spline_as_list (tools.ffspline_ivect_ref (v, i)) for i in range (n)]

cdef tools.ffspline_t make_ffspline_from_sequence (pyspline) except? NULL:
  cdef tools.ffspline_t u
  u = NULL
  for x in pyspline:
    u = tools.ffspline_push (u, x)
  return u

cdef tools.ffspline_ivect_t make_ffspline_ivect_from_sequence (pysplines) except? NULL:
  cdef double x
  cdef tools.ffspline_ivect_t v
  cdef tools.ffspline_t u
  v = NULL
  for pyspline in pysplines:
    u = NULL
    for x in pyspline:
      u = tools.ffspline_push (u, x)
    v = tools.ffspline_ivect_push (v, u)
  return v

cdef tools.ffcontour_t Point_list_to_ffcontour_t (const tools.ffcontour__ *orig,
                                                  list lst) except? NULL:
  cdef BasePoint pt
  cdef size_t i
  cdef tools.ffpoint_t p
  cdef size_t n = PyList_GET_SIZE (lst)
  cdef tools.ffcontour_t c
  c = (tools.make_ffcontour (0, 0, NULL) if orig == NULL else orig)
  for i in range (0, n):
    pt = <object> PyList_GET_ITEM (lst, i)
    Py_INCREF (pt)              # Cython will do the Py_DECREF for us.
    p = tools.make_ffpoint (pt.x__, pt.y__, pt.on_curve__, pt.selected__)
    c = tools.ffcontour_push (c, p)
  return c

cdef tools.ffcontour_t Contour_to_ffcontour_t (Contour ct) except? NULL:
  cdef BasePoint pt
  cdef size_t i
  cdef tools.ffpoint_t p
  cdef list lst = ct.points__
  cdef size_t n = PyList_GET_SIZE (lst)
  cdef tools.ffcontour_t c
  cdef bytes b
  cdef const char *c_name
  if ct.name__ is None:
    c = tools.make_ffcontour (ct.is_quadratic, ct.is_closed, NULL)
  else:
    b = to_bytes (ct.name__)
    c_name = b
    c = tools.make_ffcontour (ct.is_quadratic, ct.is_closed, c_name)
  for i in range (0, n):
    pt = <object> PyList_GET_ITEM (lst, i)
    Py_INCREF (pt)              # Cython will do the Py_DECREF for us.
    p = tools.make_ffpoint (pt.x__, pt.y__, pt.on_curve__, pt.selected__)
    c = tools.ffcontour_push (c, p)
  return c

cdef list ffcontour_t_to_Point_list (tools.ffcontour_t c):
  cdef size_t i
  cdef tools.ffpoint_t p
  cdef Point pt
  cdef size_t n = tools.ffcontour_length (c)
  cdef list lst = <list> PyList_New (n)
  cdef const tools.ffpoint_t *ptr = tools.ffcontour_ptr (c, 0)
  for i in range (0, n):
    p = ptr[0]
    pt = Point (p.x, p.y, p.on_curve, p.selected)
    Py_INCREF (pt) # This is needed because Cython is going to do a Py_DECREF.
    PyList_SET_ITEM (lst, i, pt)
    ptr = tools.ffcontour_next (c, i, ptr)
  return lst 

cdef list ffcontour_t_to_ImmutablePoint_list (tools.ffcontour_t c):
  cdef size_t i
  cdef tools.ffpoint_t p
  cdef ImmutablePoint pt
  cdef size_t n = tools.ffcontour_length (c)
  cdef list lst = <list> PyList_New (n)
  cdef const tools.ffpoint_t *ptr = tools.ffcontour_ptr (c, 0)
  for i in range (0, n):
    p = ptr[0]
    pt = ImmutablePoint (p.x, p.y, p.on_curve, p.selected)
    Py_INCREF (pt) # This is needed because Cython is going to do a Py_DECREF.
    PyList_SET_ITEM (lst, i, pt)
    ptr = tools.ffcontour_next (c, i, ptr)
  return lst 

##### cdef bint transfer_ffcontour_t_data_to_Point_list (tools.ffcontour_t c, list lst) except 0:
#####   cdef size_t i
#####   cdef tools.ffpoint_t p
#####   cdef Point pt
#####   cdef size_t n = tools.ffcontour_length (c)
#####   assert (PyList_GET_SIZE (lst) == n)
#####   cdef const tools.ffpoint_t *ptr = tools.ffcontour_ptr (c, 0)
#####   for i in range (0, n):
#####     pt = lst[i]
#####     assert (isinstance (pt, Point))
#####     p = ptr[0]
#####     pt.x__ = p.x
#####     pt.y__ = p.y
#####     pt.on_curve__ = p.on_curve
#####     pt.selected__ = p.selected
#####     ptr = tools.ffcontour_next (c, i, ptr)
#####   return 1

#####cdef bint check_that_a_sequence_is_all_contours (seq) except 0:
#####  cdef Contour ct
#####  for ct in seq:
#####    pass
#####  return 1

cdef list fflayer_t_to_ImmutableContour_list (tools.fflayer_t y):
  cdef size_t i
  cdef ImmutableContour ct
  cdef size_t n = tools.fflayer_length (y)
  cdef list lst = <list> PyList_New (n)
  cdef const tools.ffcontour_t *ptr = tools.fflayer_ptr (y, 0)
  for i in range (0, n):
    ct = ImmutableContour ()
    ct.root[0] = ptr[0]
    Py_INCREF (ct) # This is needed because Cython is going to do a Py_DECREF.
    PyList_SET_ITEM (lst, i, ct)
    ptr = tools.fflayer_next (y, i, ptr)
  return lst 

#--------------------------------------------------------------------------
#
# Information about the package.
#

class package_info:
  package = PACKAGE
  package_bugreport = str (PACKAGE_BUGREPORT)
  package_name = str (PACKAGE_NAME)
  package_string = str (PACKAGE_STRING)
  package_tarname = str (PACKAGE_TARNAME)
  package_url = str (PACKAGE_URL)
  package_version = str (PACKAGE_VERSION)
  version_major = str (VERSION_MAJOR)
  version_minor = str (VERSION_MINOR)
  version_patch = str (VERSION_PATCH)
  version_extra = str (VERSION_EXTRA)
  version_extra_short = str (VERSION_EXTRA_SHORT)
  version = version_major + '.' + version_minor + '.' + \
            version_patch + version_extra_short

__version__ = package_info.version

#--------------------------------------------------------------------------
#
# Some deprecated leftovers from FontForge.
#

def version ():
  warnings.warn ('version() is deprecated; use __version__ instead.',
                 DeprecationWarning)
  # A workaround: return a big value so, in old scripts, checking for
  # a minimal version always succeeds.
  return '99999999'

def loadPlugin (filename):
  warnings.warn ('loadPlugin() is deprecated in {} and does nothing.'
                 .format(package_info.package_name),
                 DeprecationWarning)

def loadPluginDir (dirname):
  warnings.warn ('loadPluginDir() is deprecated in {} and does nothing.'
                 .format(package_info.package_name),
                 DeprecationWarning)

def hasSpiro ():
  warnings.warn ('hasSpiro() is always True in {}.'
                 .format(package_info.package_name),
                 DeprecationWarning)
  return True

#--------------------------------------------------------------------------

def parseTTInstrs (string):
  cdef bytes b = to_bytes (string)
  cdef tools.ttf_instructions *instr = tools.parse_ttf_instructions (b)
  if instr.error_message != NULL:
    #
    # FIXME: One might add the offset to the message.
    #
    # NOTE: FontForge had ‘TypeError’ here.
    #
    raise ValueError ("Failed to parse TrueType instructions: " +
                      instr.error_message)
  cdef bytes result = instr.instructions[:instr.num_instructions]
  return result

def unParseTTInstrs (sequence):
  cdef tools.ttf_instructions instr
  cdef size_t i
  instr.num_instructions = len (sequence)
  if isinstance (sequence, bytes):
    instr.instructions = sequence
  else:
    instr.instructions = <uint8_t *> \
                         core.x_gc_malloc_atomic (instr.num_instructions)
    for i in range (instr.num_instructions):
      instr.instructions[i] = <uint8_t> sequence[i]
  cdef char *result = tools.unparse_ttf_instructions (&instr)
  return to_str (result)

#--------------------------------------------------------------------------
#
# xyTuple: a pair of objects, with the objects to be associated,
# respectively, with the x-axis and the y-axis.
#
# NOTE: DO NOT ALTER THE ORDER OF x AND y. It is allowed to assume the
# x-element has position 0 and the y-element position 1.
#

xyTuple = collections.namedtuple ("xyTuple", "x y")

#--------------------------------------------------------------------------
#
# Hobby guides: specification of cubic splines by the methods of
#               Metafont and Metapost.
#
#
# For example, a clockwise circle of radius 100, centered at (100,0),
# and having on-curve extreme points:
#
#    import sortsmill.ffcompat as ff
#    guide = [ff.hpoint(0,0),
#             ff.up(),
#             ff.tension(".."),
#             ff.hpoint (100,100),
#             ff.right(),
#             ff.tension(".."),
#             ff.hpoint(200,0),
#             ff.down(),
#             ff.tension(".."),
#             ff.hpoint(100,-100),
#             ff.left(),
#             ff.tension(".."),
#             ff.cycle()];
#    print(ff.ImmutableContour.fromHobbyGuide(guide).round(1e5))'
#
# This should print:
#
#    ImmutableContour(
#       is_quadratic=False,
#       is_closed=True,
#       name=None,
#       points=[
#          ImmutablePoint(x=0.0, y=0.0, on_curve=True, selected=False),
#          ImmutablePoint(x=0.0, y=55.22847, on_curve=False, selected=False),
#          ImmutablePoint(x=44.77153, y=100.0, on_curve=False, selected=False),
#          ImmutablePoint(x=100.0, y=100.0, on_curve=True, selected=False),
#          ImmutablePoint(x=155.22847, y=100.0, on_curve=False, selected=False),
#          ImmutablePoint(x=200.0, y=55.22847, on_curve=False, selected=False),
#          ImmutablePoint(x=200.0, y=0.0, on_curve=True, selected=False),
#          ImmutablePoint(x=200.0, y=-55.22847, on_curve=False, selected=False),
#          ImmutablePoint(x=155.22847, y=-100.0, on_curve=False, selected=False),
#          ImmutablePoint(x=100.0, y=-100.0, on_curve=True, selected=False),
#          ImmutablePoint(x=44.77153, y=-100.0, on_curve=False, selected=False),
#          ImmutablePoint(x=0.0, y=-55.22847, on_curve=False, selected=False)
#       ]
#    )
#
# You can also use a string as your Hobby guide. The string will be
# converted to a Scheme list and used as input to the
# ‘solve-hobby-guide’ procedure of Sorts Mill Core Guile. For example,
# the same circle as above:
#
#    import sortsmill.ffcompat as ff
#    guide = "(0 0) up .. (100 100) right .. (200 0) down .. (100 -100) left .. cycle"
#    print(ff.ImmutableContour.fromHobbyGuide(guide).round(1e5))'
#
# (Side note: you could leave out the ‘up’, ‘right’, ‘down’, ‘left’
# direction indicators and still get the same circle, to within minor
# arithmetic differences.)
#
#

# Token values for ‘raw’ token sequence input.
HOBBY_GUIDE_POINT = 0
HOBBY_GUIDE_CYCLE = 1
HOBBY_GUIDE_TENSION = 2
HOBBY_GUIDE_ATLEAST = 3
HOBBY_GUIDE_DIR = 4
HOBBY_GUIDE_CURL = 5
HOBBY_GUIDE_CTRL = 6

# Possible values of the "info" entry in exceptions raised by the
# BaseContour.fromHobbyGuide method.
HOBBY_GUIDE_TOKEN_ERROR = -1
HOBBY_GUIDE_SOLUTION_NOT_FOUND = -2

# MF_INFINITY: Metafont’s idea of ‘infinity’. By contrast, our idea of
# ‘infinity’ is HUGE_VAL from <math.h>, which should be an IEEE
# ‘infinity’. However, you can use MF_INFINITY if you want.
MF_INFINITY = 4095.99998

def hpoint (x, y = None):
  """Return an on-curve point for inclusion in a Hobby guide. You may
  specify two real numbers as xy-coordinates, or a single complex
  number or real-number sequence.

  """
  cdef double x0
  cdef double y0
  if y is None:
    if isinstance (x, numbers.Complex):
      x0 = x.real
      y0 = x.imag
    else:
      x0 = x[0]
      y0 = x[1]
  else:
    x0 = x
    y0 = y
  return (HOBBY_GUIDE_POINT, x0, y0)

def cycle ():
  """Return a "cycle" token for inclusion in a Hobby guide."""
  return (HOBBY_GUIDE_CYCLE, 0, 0)

def controls (x0y0, x1y1):
  cdef double x0
  cdef double y0
  cdef double x1
  cdef double y1
  if (x0y0 is None
      or x1y1 is None
      or isinstance (x0y0, numbers.Real)
      or isinstance (x1y1, numbers.Real)):
    raise ValueError ("Expected two complex numbers or real-number sequences as arguments.")
  if isinstance (x0y0, numbers.Complex):
    x0 = x0y0.real
    y0 = x0y0.imag
  else:
    x0 = x0y0[0]
    y0 = x0y0[1]
  if isinstance (x1y1, numbers.Complex):
    x1 = x1y1.real
    y1 = x1y1.imag
  else:
    x1 = x1y1[0]
    y1 = x1y1[1]
  return (HOBBY_GUIDE_CTRL, x0, y0, HOBBY_GUIDE_CTRL, x1, y1)

def tension (t1, t2 = None, atleast = False, enforce_bound = True):
  # FIXME: Add a docstring.
  cdef double t1_
  cdef double t2_
  hobby_guide_tension = (HOBBY_GUIDE_ATLEAST if atleast else HOBBY_GUIDE_TENSION)
  if t2 is None:
    if isinstance (t1, numbers.Real):
      t1_ = t1
      t2_ = t1
    elif to_str (t1) == "..":
      t1_ = 1.0
      t2_ = 1.0
    elif to_str (t1) == "...":
      hobby_guide_tension = HOBBY_GUIDE_ATLEAST
      t1_ = 1.0
      t2_ = 1.0
    elif to_str (t1) == "--":
      t1_ = HUGE_VAL
      t2_ = HUGE_VAL
    elif to_str (t1) == "---":
      t1_ = MF_INFINITY
      t2_ = MF_INFINITY
  else:
    t1_ = t1
    t2_ = t2
  if enforce_bound and (t1_ < 0.75 or t2_ < 0.75):
    raise ValueError ("tension less than 0.75")
  return (hobby_guide_tension, t1_, t2_)

def tension_atleast (t1, t2 = None, enforce_bound = True):
  # FIXME: Add a docstring.
  return tension (t1, t2, atleast = True, enforce_bound = enforce_bound)

def atleast (t1, t2 = None, enforce_bound = True):
  # FIXME: Add a docstring.
  return tension_atleast (t1, t2, atleast = True, enforce_bound = enforce_bound)

def infinite_tension ():
  # FIXME: Add a docstring.
  return (HOBBY_GUIDE_TENSION, HUGE_VAL, HUGE_VAL)

def direction (x, y = None):
  # FIXME: Add a docstring.
  cdef double x0
  cdef double y0
  if y is None:
    if isinstance (x, numbers.Complex):
      x0 = x.real
      y0 = x.imag
    else:
      x0 = x[0]
      y0 = x[1]
  else:
    x0 = x
    y0 = y
  return (HOBBY_GUIDE_DIR, x0, y0)

@cython.cdivision(True)
def angle (double phi, radians = False):
  # FIXME: Add a docstring.
  cdef double x0
  cdef double y0
  if not radians:
    # Convert from degrees to radians.
    phi = (M_PI * phi) / 180.0
  x0 = cos (phi)
  y0 = sin (phi)
  return (HOBBY_GUIDE_DIR, x0, y0)

def left ():
  # FIXME: Add a docstring.
  return (HOBBY_GUIDE_DIR, -1.0, 0.0)

def up ():
  # FIXME: Add a docstring.
  return (HOBBY_GUIDE_DIR, 0.0, 1.0)

def right ():
  # FIXME: Add a docstring.
  return (HOBBY_GUIDE_DIR, 1.0, 0.0)

def down ():
  # FIXME: Add a docstring.
  return (HOBBY_GUIDE_DIR, 0.0, -1.0)

def curl (double c, enforce_bound = True):
  # FIXME: Add a docstring.
  if enforce_bound and c < 0:
    raise ValueError ("curl less than zero")
  return (HOBBY_GUIDE_CURL, c, 0)

#--------------------------------------------------------------------------
#
# Conversion between control-point and direction-and-tension
# representations of cubic splines.
#

def cubicSplinesFromTensions (p0, dir0, double tension0, double tension3,
                              dir3, p3, bint perhaps_increase_tensions = False,
                              bint radians = False):
  # FIXME: Write a docstring.
  cdef tools.ffspline_t x
  cdef tools.ffspline_t y
  cdef double dir0x
  cdef double dir0y
  cdef double dir3x
  cdef double dir3y
  cdef double x0 = p0[0]
  cdef double y0 = p0[1]
  cdef double x3 = p3[0]
  cdef double y3 = p3[1]

  if isinstance (dir0, numbers.Real):
    if radians:
      dir0x = cos (dir0)
      dir0y = sin (dir0)
    else:
      dir0x = cos (dir0 * M_PI / 180.0)
      dir0y = sin (dir0 * M_PI / 180.0)
  else:
    dir0x = dir0[0]
    dir0y = dir0[1]

  if isinstance (dir3, numbers.Real):
    if radians:
      dir3x = cos (dir3)
      dir3y = sin (dir3)
    else:
      dir3x = cos (dir3 * M_PI / 180.0)
      dir3y = sin (dir3 * M_PI / 180.0)
  else:
    dir3x = dir3[0]
    dir3y = dir3[1]

  tools.tensions_to_ffspline_cubic (perhaps_increase_tensions,
                                      x0, y0, x3, y3, dir0x, dir0y, dir3x, dir3y,
                                      tension0, tension3, &x, &y)

  return xyTuple (make_spline_as_list (x), make_spline_as_list (y))

cpdef tuple tensionsFromCubicSplines (x, y = None):
  # FIXME: Write a docstring.
  if y is None:
    y = x[1]
    x = x[0]
  cdef double tension0
  cdef double tension3
  cdef tools.ffspline_t x_ = make_ffspline_from_sequence (x)
  cdef tools.ffspline_t y_ = make_ffspline_from_sequence (y)
  if tools.ffspline_length (x_) != 4 or tools.ffspline_length (y_) != 4:
    raise ValueError ("Expected cubic splines.")
  tools.ffspline_cubic_tensions (x_, y_, &tension0, &tension3)
  return (tension0, tension3)

#--------------------------------------------------------------------------

cdef class BasePoint:

  """A parent class for ImmutablePoint and Point."""

  cdef double x__
  cdef double y__
  cdef bint on_curve__
  cdef bint selected__

  @property
  def x (self):
    return self.x__

  @property
  def y (self):
    return self.y__

  @property
  def on_curve (self):
    return self.on_curve__

  @property
  def selected (self):
    return self.selected__

  def __init__ (self, x = None, y = None, on_curve = None, selected = None):
    """Construct the object from primitive values, another
    BasePoint, and/or a sequence.

    """
    if x is None or isinstance (x, numbers.Number):
      self.x__ = (0.0 if x is None else x)
      self.y__ = (0.0 if y is None else y)
      self.on_curve__ = (True if on_curve is None else on_curve)
      self.selected__ = (False if selected is None else selected)
    elif isinstance (x, BasePoint):
      # It is not allowed to call with extra arguments if the first
      # argument is a BasePoint.
      check_precondition (y is None and on_curve is None and selected is None)
      self.x__ = x.x
      self.y__ = x.y
      self.on_curve__ = x.on_curve
      self.selected__ = x.selected
    else:
      # It is not allowed to call with extra arguments if the argument
      # is a sequence.
      check_precondition (y is None and on_curve is None and selected is None)
      # It is not allowed for the sequence to be too long.
      n = len (x)
      check_precondition (n < 5)
      self.x__ = x[0]
      self.y__ = x[1]
      self.on_curve__ = (True if n < 3 else x[2])
      self.selected__ = (False if n < 4 else x[3])

  def __getitem__ (self, ssize_t key):
    """Return an attribute of the BasePoint, specified by a key. Possible
    values of the key:

       (0 or -4) = x

       (1 or -3) = y

       (2 or -2) = on_curve

       (3 or -1) = selected

    """
    if key == 0 or key == -4:
      result = self.x__
    elif key == 1 or key == -3:
      result = self.y__
    elif key == 2 or key == -2:
      result = self.on_curve__
    elif key == 3 or key == -1:
      result = self.selected__
    else:
      raise IndexError ()
    return result

  def __len__ (self):
    return 4

  def __iter__ (self):
    yield self.x__
    yield self.y__
    yield self.on_curve__
    yield self.selected__

  def __reversed__ (self):
    yield self.selected__
    yield self.on_curve__
    yield self.y__
    yield self.x__

  def __repr__ (self):
    return "{}(x={}, y={}, on_curve={}, selected={})" \
      .format (type(self).__name__, repr (self.x__), repr (self.y__),
               repr (self.on_curve__), repr (self.selected__))

  cpdef BasePoint transformed_by (self, tupl, bint with_rounding = 1):
    # FIXME: Document with_rounding.
    """Return a copy of the BasePoint, transformed by the PostScript-style
    transformation matrix (a 6-element sequence of numbers)

    """
    cdef tools.ffpoint_t p
    cdef tools.ffpsmat_t a
    p = tools.make_ffpoint (self.x__, self.y__, self.on_curve__, self.selected__)
    a = tools.make_ffpsmat (tupl[0], tupl[1], tupl[2], tupl[3], tupl[4], tupl[5])
    if with_rounding:
      p = tools.ffpoint_transform (p, a)
    else:
      p = tools.ffpoint_transform_without_rounding (p, a)
    return type(self) (p.x, p.y, p.on_curve, p.selected)

  def __richcmp__ (BasePoint p, q, int operation):
    cdef tools.ffpoint_t p_
    cdef tools.ffpoint_t q_
    cdef double qx
    cdef double qy
    cdef bint q_on_curve
    cdef bint q_selected
    p_ = tools.make_ffpoint (p.x, p.y, p.on_curve, p.selected)
    if isinstance (q, BasePoint):
      qx = q.x
      qy = q.y
      q_on_curve = q.on_curve
      q_selected = q.selected
    else:
      qx = q[0]
      qy = q[1]
      q_on_curve = p.on_curve
      q_selected = p.selected
    q_ = tools.make_ffpoint (qx, qy, q_on_curve, q_selected)
    return cmp_to_richcmp (tools.ffpoint_compare (p_, q_), operation)

  def __reduce__ (self):
    """Reduce the object for pickling."""
    return (self.__class__, (self.x__, self.y__, self.on_curve__, self.selected__))

#--------------------------------------------------------------------------

cdef class ImmutablePoint (BasePoint):

  """A point in a plane contour"""

  cpdef ImmutablePoint set_x (self, double x):
    """Return a new ImmutablePoint with the x-value changed."""
    return ImmutablePoint (x, self.y__, self.on_curve__, self.selected__)

  cpdef ImmutablePoint set_y (self, double y):
    """Return a new ImmutablePoint with the y-value changed."""
    return ImmutablePoint (self.x__, y, self.on_curve__, self.selected__)

  cpdef ImmutablePoint set_on_curve (self, bint on_curve):
    """Return a new ImmutablePoint with the on_curve-value changed."""
    return ImmutablePoint (self.x__, self.y__, on_curve, self.selected__)

  cpdef ImmutablePoint set_selected (self, bint selected):
    """Return a new ImmutablePoint with the selected-value changed."""
    return ImmutablePoint (self.x__, self.y__, self.on_curve__, selected)

  cpdef get (self, ssize_t key):
    """A synonym for __getitem__."""
    return self.__getitem__ (key)

  cpdef ImmutablePoint set (self, ssize_t key, value):
    """Return a new ImmutablePoint with an attribute specified by the key
    changed. Possible values of the key:

       (0 or -4) = x

       (1 or -3) = y

       (2 or -2) = on_curve

       (3 or -1) = selected

    """
    if key == 0 or key == -4:
      result = self.set_x (value)
    elif key == 1 or key == -3:
      result = self.set_y (value)
    elif key == 2 or key == -2:
      result = self.set_on_curve (value)
    elif key == 3 or key == -1:
      result = self.set_selected (value)
    else:
      raise IndexError ()
    return result

  cpdef ImmutablePoint transform (self, tupl, bint with_rounding = 1):
    """A synonym for transformed_by."""
    return self.transformed_by (tupl, with_rounding)

#--------------------------------------------------------------------------

cdef class Point (BasePoint):

  """A point in a plane contour"""

  @property
  def x (self):
    return self.x__

  @x.setter
  def x (self, value):
    self.x__ = value

  @property
  def y (self):
    return self.y__

  @y.setter
  def y (self, value):
    self.y__ = value

  @property
  def on_curve (self):
    return self.on_curve__

  @on_curve.setter
  def on_curve (self, value):
    self.on_curve__ = value

  @property
  def selected (self):
    return self.selected__

  @selected.setter
  def selected (self, value):
    self.selected__ = value

  def __str__ (self):
    return "<FFPoint ({}, {}) {}>".format (self.x__, self.y__,
                                           ("on" if self.on_curve__ else "off"))

  def __setitem__ (self, ssize_t key, value):
    # A Sorts Mill FFCompat extension.
    """Set an attribute of the Point, specified by a key. Possible values
    of the key:

       (0 or -4) = x

       (1 or -3) = y

       (2 or -2) = on_curve

       (3 or -1) = selected

    """
    if key == 0 or key == -4:
      self.x__ = value
    elif key == 1 or key == -3:
      self.y__ = value
    elif key == 2 or key == -2:
      self.on_curve__ = value
    elif key == 3 or key == -1:
      self.selected__ = value
    else:
      raise IndexError ()

  cpdef Point dup (self):
    """Return a copy of the Point. (You can also say, for instance,
    "Point(p)" to duplicate a Point named p.)

    """
    return Point (self.x__, self.y__, self.on_curve__, self.selected__)

  cpdef Point transform (self, tupl, bint with_rounding = 1):
    # FIXME: Document with_rounding.
    """Transform the Point, in place, by the PostScript-style
    transformation matrix (a 6-element sequence of numbers).

    """
    cdef tools.ffpoint_t p
    cdef tools.ffpsmat_t a
    p = tools.make_ffpoint (self.x__, self.y__, self.on_curve__, self.selected__)
    a = tools.make_ffpsmat (tupl[0], tupl[1], tupl[2], tupl[3], tupl[4], tupl[5])
    if with_rounding:
      p = tools.ffpoint_transform (p, a)
    else:
      p = tools.ffpoint_transform_without_rounding (p, a)
    self.x__ = p.x
    self.y__ = p.y
    self.on_curve__ = p.on_curve
    self.selected__ = p.selected
    return self

#--------------------------------------------------------------------------

cdef class BaseContour:

  """A parent class for contours."""

  cdef tools.ffcontour_t to_ffcontour (self):
    cdef ImmutableContour ct = ImmutableContour (self)
    return ct.root[0]

  cdef ImmutableContour to_ImmutableContour (self):
    return ImmutableContour (self)

  cdef ImmutableLayer to_ImmutableLayer (self):
    return ImmutableLayer (self.is_quadratic, [self])

  def __init__ (self, *args, **kwargs):
    raise NotImplementedError ()

  def __repr__ (self):
    # FIXME: There is no support for spiros here.
    return "{}(is_quadratic={}, is_closed={}, name={}, points={})" \
      .format (type(self).__name__, repr (self.is_quadratic), repr (self.is_closed),
               repr (self.name), repr (self.points))

  def __str__ (self):
    # FIXME: There is no support for spiros here.
    #
    # This definition of __str__ gives the same result as __repr__,
    # but with ‘structured’ whitespace.
    #
    cdef BasePoint p
    cdef str s
    if len (self.points) == 0:
      s = "[]"
    else:
      s = "[\n"
      for p in self.points[:-1]:
        s += "      " + repr (p) + ",\n"
      s += "      " + repr(self.points[-1]) + "\n   ]"
    return "{}(\n   is_quadratic={},\n   is_closed={},\n   name={},\n   points={}\n)\n" \
      .format (type(self).__name__, repr (self.is_quadratic), repr (self.is_closed),
               repr (self.name), s)

  def __reduce__ (self):
    # FIXME: Currently there is no support for spiros here.
    """Reduce the object for pickling."""
    return (self.__class__,
            (self.is_quadratic, self.is_closed, self.name,
             [(p.x, p.y, p.on_curve, p.selected) for p in self],
             None,  # A placeholder for possible future spiros support.
             None   # For the "closed" keyword argument.
            ))

  @property
  def is_quadratic (self):
    raise NotImplementedError ()

  @property
  def is_closed (self):
    raise NotImplementedError ()

  @property
  def closed (self):
    raise NotImplementedError ()

  @property
  def name (self):
    raise NotImplementedError ()

  @property
  def points (self):
    """Return a shallow or deep copy of the points of the contour, as a
    list. Whether the copy is shallow or deep depends on the subclass."""
    raise NotImplementedError ()

  cpdef object findNextOnCurve (self, ssize_t i):
    """Find the index of the next on-curve point after the ith, or return
    None if there is none. Searches within closed Contours loop
    around.

    """
    cdef tools.ffcontour_t c = self.to_ffcontour ()
    cdef size_t n = tools.ffcontour_length (c)
    cdef ssize_t j = tools.ffcontour_find_next_on_curve (c, fold_index (n, i))
    return (None if j < 0 else j)

  cpdef object findPrevOnCurve (self, ssize_t i):
    """Find the index of the previous on-curve point before the ith, or
    return None if there is none. Searches within closed contours loop
    around.

    """
    cdef tools.ffcontour_t c = self.to_ffcontour ()
    cdef size_t n = tools.ffcontour_length (c)
    cdef ssize_t j = tools.ffcontour_find_prev_on_curve (c, fold_index (n, i))
    return (None if j < 0 else j)

  cpdef object findNextOffCurve (self, ssize_t i):
    """Find the index of the next off-curve point after the ith, or return
    None if there is none. Searches within closed contours loop
    around.

    """
    cdef tools.ffcontour_t c = self.to_ffcontour ()
    cdef size_t n = tools.ffcontour_length (c)
    cdef ssize_t j = tools.ffcontour_find_next_off_curve (c, fold_index (n, i))
    return (None if j < 0 else j)

  cpdef object findPrevOffCurve (self, ssize_t i):
    """Find the index of the previous off-curve point before the ith, or
    return None if there is none. Searches within closed contours loop
    around.

    """
    cdef tools.ffcontour_t c = self.to_ffcontour ()
    cdef size_t n = tools.ffcontour_length (c)
    cdef ssize_t j = tools.ffcontour_find_prev_off_curve (c, fold_index (n, i))
    return (None if j < 0 else j)

  cpdef int isClockwise (self):
    #
    # FIXME: THIS METHOD DOES NOT WORK.
    # FIXME: THIS METHOD DOES NOT WORK.
    # FIXME: THIS METHOD DOES NOT WORK.
    # FIXME: THIS METHOD DOES NOT WORK.
    # FIXME: THIS METHOD DOES NOT WORK.
    # FIXME: THIS METHOD DOES NOT WORK.
    # FIXME: THIS METHOD DOES NOT WORK.
    #
    """Returns 1 if the contour is drawn in a clockwise direction, 0 if it
    is counterclockwise, and -1 if no consistent direction could be
    found (for example, if the contour is empty, open, or intersects
    itself).

    """
    a = "a"
    if a == "a":    # Avoid an ‘Unreachable code’ warning from Cython.
      raise RuntimeError ("The Contour.isClockwise method currently is not working correctly.")

    cdef tools.ffcontour_t c = self.to_ffcontour ()
    cdef int orientation = tools.ffcontour_orientation (c)
    cdef int is_clockwise
    if orientation < 0:
      is_clockwise = 1          # clockwise
    elif orientation == 0:
      is_clockwise = -1;        # indeterminate
    else:
      is_clockwise = 0;         # counterclockwise
    return is_clockwise

  def xBoundsAtY (self, double ybottom, ytop = None):
    """Find the minimum and maximum x positions attained by the contour
    when y is between ybottom and ytop (if ytop is not specified it is
    assumed the same as ybottom); return them as a tuple. If the
    contour does not have any y values in the specified range then
    return None.

    """
    cdef double ybound1 = ybottom
    cdef double ybound2

    if ytop is None:
      ybound2 = ybound1
    else:
      ybound2 = ytop

    cdef tools.ffcontour_t c = self.to_ffcontour ()

    cdef int bounds_found
    cdef double xmin
    cdef double xmax
    cdef const char *error_message
    tools.ffcontour_x_bounds_at_y (c, ybound1, ybound2,
                                       &bounds_found, &xmin, &xmax,
                                       &error_message)
    if error_message != NULL:
      raise RuntimeError (to_str (error_message))
    if (<bint> bounds_found):
      bounds = (xmin, xmax)
    else:
      bounds = None
    return bounds

  def yBoundsAtX (self, double xleft, xright = None):
    """Find the minimum and maximum y positions attained by the contour
    when x is between xleft and xright (if xright is not specified it
    is assumed the same as xleft); return them as a tuple. If the
    contour does not have any x values in the specified range then
    return None.

    """
    cdef double xbound1 = xleft
    cdef double xbound2

    if xright is None:
      xbound2 = xbound1
    else:
      xbound2 = xright

    cdef tools.ffcontour_t c = self.to_ffcontour ()

    cdef int bounds_found
    cdef double ymin
    cdef double ymax
    cdef const char *error_message
    tools.ffcontour_y_bounds_at_x (c, xbound1, xbound2,
                                       &bounds_found, &ymin, &ymax,
                                       &error_message)
    if error_message != NULL:
      raise RuntimeError (to_str (error_message))
    if (<bint> bounds_found):
      bounds = (ymin, ymax)
    else:
      bounds = None
    return bounds

  cpdef tuple boundingBox (self):
    """Return a tuple representing a rectangle (xmin,ymin,xmax,ymax) into
    which the contour fits. It is not guaranteed to be the smallest
    such rectangle, but often it will be. It will be the smallest
    vertically and horizontally aligned rectangle into which the
    control points all fit.

    If the contour is empty, the tuple (0.0, 0.0, 0.0, 0.0) will be
    returned."""
    cdef tools.ffcontour_bounding_box_t box
    cdef tools.ffcontour_t c = self.to_ffcontour ()
    box = tools.ffcontour_bounding_box (c)
    return (box.x_min, box.y_min, box.x_max, box.y_max)

  cpdef bint selfIntersects (self) except? 1:
    """Does the contour intersect itself?"""
    cdef int is_self_intersecting
    cdef const char *error_message
    cdef tools.ffcontour_t c = self.to_ffcontour ()
    tools.ffcontour_is_self_intersecting (c, &is_self_intersecting,
                                          &error_message)
    if error_message != NULL:
      raise RuntimeError (to_str (error_message))
    return <bint> is_self_intersecting

  cpdef tuple getSplineAfterPoint (self, ssize_t pos):
    """Return a tuple of two four-element tuples. These tuples are x and y
    splines for the curve after the specified point. (In Sorts Mill
    FFCompat, getSplineAfterPoint accepts negative indexes.)"""
    warnings.warn ('Use toSplines instead of getSplineAfterPoint',
                   DeprecationWarning)
    cdef tools.ffcontour_t c = self.to_ffcontour ()
    cdef size_t n = tools.ffcontour_length (c)
    cdef tools.ffcontour_spline_after_point_t sap
    sap = tools.ffcontour_spline_after_ffpoint (c, fold_index (n, pos))
    return ((sap.x[0], sap.x[1], sap.x[2], sap.x[3]),
            (sap.y[0], sap.y[1], sap.y[2], sap.y[3]))

  def draw (self, pen):
    # FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME.
    raise RuntimeError ("BaseContour.draw currently is not implemented in Sorts Mill FFCompat.")

  cpdef bint similar (self, BaseContour other,
                      double point_error = -1.0,
                      double spline_error = -1.0):
    # FIXME: Check this documentation against actual behavior. In
    # FontForge, the HTML documentation does not match the
    # implementation!
    """Check whether this contour is similar to the other one, where the
    error arguments give the maximum distance (in em-units) allowed
    for the two contours to diverge.

    This is like the comparison operator, but that doesn't allow you
    to specify an error bound.

    """
    cdef const char *error_message1
    cdef const char *error_message2
    cdef int the_contours_are_similar
    cdef tools.ffcontour_t ffc1 = self.to_ffcontour ()
    cdef tools.ffcontour_t ffc2 = other.to_ffcontour ()

    tools.ffcontour_similarity (ffc1, ffc2, point_error, spline_error,
                                        &the_contours_are_similar,
                                        &error_message1, &error_message2)
    if error_message1 != NULL:
      raise ValueError('"self": ' + to_str (error_message1))
    if error_message2 != NULL:
      raise ValueError('"other": ' + to_str (error_message2))
    return <bint> the_contours_are_similar

  cdef int cmp_internal__ (BaseContour c1, BaseContour c2):

    cdef ImmutableContour ic1 = c1.to_ImmutableContour ()
    cdef ImmutableContour ic2 = c2.to_ImmutableContour ()

    cdef tools.ffcontour_t c1_
    cdef tools.ffcontour_t c2_
    cdef int cmp_value = 0
    cdef size_t i

    if not c1.similar (ic2, 0.5, 1.0):
      # The following seems pretty silly to me, but it is what
      # FontForge does. Perhaps FontForge should have raised
      # NotImplemented, instead.
      c1_ = ic1.root[0]
      c2_ = ic2.root[0]
      if tools.ffcontour_length (c1_) < tools.ffcontour_length (c2_):
        cmp_value = -1
      elif tools.ffcontour_length (c2_) < tools.ffcontour_length (c1_):
        cmp_value = 1
      else:
        i = 0
        cmp_value = 0
        while cmp_value == 0 and i < tools.ffcontour_length (c1_):
          cmp_value = tools.ffpoint_compare (tools.ffcontour_ref (c1_, i),
                                             tools.ffcontour_ref (c2_, i))
          i += 1
        if tools.ffcontour_length (c1_) <= i:
          cmp_value = -1

    return cmp_value

  def __richcmp__ (BaseContour c1, c2, int operation):
    cdef int cmp_value
    if isinstance (c2, BaseContour):
      cmp_value = c1.cmp_internal__ (c2)
    elif isinstance (c2, BaseLayer):
      if len (c2) == 1:
        cmp_value = c1.cmp_internal__ (c2[0])
      elif len (c2) == 0:
        cmp_value = 1
      else:
        cmp_value = -1
    else:
      raise TypeError ()
    return cmp_to_richcmp (cmp_value, operation)

  cpdef BaseContour transformed_by (self, matrix, bint with_rounding = 1):
    # NOTE: transformed_by is a Sorts Mill extension.
    #
    # FIXME: Document with_rounding.
    #
    """Return a copy of the Contour, transformed by the PostScript-style
    transformation matrix (a 6-element sequence of numbers).

    """
    cdef ImmutableContour ic = self.to_ImmutableContour ()
    ic = ic.transformed_by (matrix, with_rounding)
    return type(self) (ic)

  def toSplines (self):
    #
    # A Sorts Mill extension.
    #
    # FIXME: Write better documentation, perhaps with pictures.
    """Get the splines of the contour. Each x or y spline will be
    represented as a list of 3 or 4 floating point numbers, depending
    on the degree of the contour; the numbers are spline coordinates
    in the Bernstein basis. The x and y coordinate-lists are gathered
    respectively into lists of lists. The returned value is an
    "xyTuple" object, with either element of the tuple being one of
    the lists of lists.

    The splines will be listed in piecewise order, starting with the
    first on-curve point of the contour. If the contour is open, it
    must start and end with on-curve point objects. If the contour is
    quadratic, the first on-curve Point might be a TrueType-style
    interpolated point.

    If the splines cannot be constructed, an exception will be
    raised.

    """
    cdef tools.ffspline_ivect_t x
    cdef tools.ffspline_ivect_t y
    cdef const char *error_message
    cdef tools.ffcontour_t c = self.to_ffcontour ()
    tools.ffcontour_to_ffsplines (c, &x, &y, &error_message)
    if (error_message != NULL):
      raise RuntimeError (to_str (error_message))
    return xyTuple (make_list_of_splines (x), make_list_of_splines (y))

  @classmethod
  def fromSplines (result_class, splines, bint is_quadratic, bint is_closed,
                    name = None):
    #
    # A Sorts Mill extension.
    #
    # FIXME: Write better documentation.
    #
    """Convert splines to a contour."""

    cdef bytes b
    cdef const char *name_ = NULL
    if name is not None:
      b = to_bytes (name)
      name_ = b

    x_splines = splines[0]
    y_splines = splines[1]
    cdef tools.ffspline_ivect_t x = make_ffspline_ivect_from_sequence (x_splines)
    cdef tools.ffspline_ivect_t y = make_ffspline_ivect_from_sequence (y_splines)
    cdef tools.ffcontour_t c
    cdef const char *error_message
    tools.ffsplines_to_ffcontour (x, y, is_quadratic, is_closed, name_,
                                  &c, &error_message)

    if error_message != NULL:
      raise ValueError (to_str (error_message))

    cdef ImmutableContour ict = ImmutableContour ()
    ict.root[0] = c
    return result_class (ict)

  @classmethod
  def fromHobbyGuide (result_class, hobby_guide, name = None,
                      infinite_tensions_become_lines = True):
    # FIXME: Better document this.
    """Make a new cubic contour from a Hobby guide."""

    def raise_again_with_subsequence_indices (hobby_guide, err, bad_token_index):
      fine_grain_index = 3 * bad_token_index
      i = 0
      n = 0
      while n < fine_grain_index:
        n += len (hobby_guide[i])
        i += 1
      if n == fine_grain_index:
        err.args[0]["subsequence_index"] = i
        err.args[0]["index_within_subsequence"] = 0
      else:
        err.args[0]["subsequence_index"] = i - 1
        err.args[0]["index_within_subsequence"] = (fine_grain_index -
                                                   (n - len (hobby_guide[i - 1])))
      raise err

    cdef bytes b
    cdef const char *name_ = NULL
    if name is not None:
      b = to_bytes (name)
      name_ = b

    cdef size_t i
    cdef size_t n
    cdef double *input_tokens

    cdef const char *guide_string

    cdef tools.ffcontour_t c
    cdef ImmutableContour ict

    cdef int info
    cdef const char *info_message
    cdef size_t bad_token_index

    if not isinstance (hobby_guide, collections.Sequence):
      raise ValueError ("Expected a sequence as Hobby guide.")
    if len (hobby_guide) == 0:
      # Avoid the issue of whether an empty Hobby guide should be
      # regarded as open or closed, by simply saying there are no such
      # things as empty Hobby guides. :)
      raise ValueError ("Expected a non-empty sequence as Hobby guide.")
    elif isinstance (hobby_guide, str):
      #
      # The input is a string representing a Hobby guide in Scheme.
      #
      b = to_bytes (hobby_guide)
      guide_string = b
      tools.scheme_hobby_guide_to_ffcontour (guide_string, name_,
                                             <bint> infinite_tensions_become_lines,
                                             &c, &info_message)
      if info_message != NULL:
        raise RuntimeError (to_str (info_message))
      ict = ImmutableContour ()
      ict.root[0] = c
      result = result_class (ict)
    elif all (isinstance (x, numbers.Real) for x in hobby_guide):
      #
      # The input is a token sequence.
      #
      token_sequence = hobby_guide
      n = len (token_sequence)
      if (n % 3) != 0:
        raise ValueError ("The length of a Hobby guide token sequence must be divisible by 3.")
      input_tokens = <double *> core.x_gc_malloc_atomic (n * sizeof (double))
      for i in range (0, n):
        input_tokens[i] = hobby_guide[i]
      tools.hobby_guide_to_ffcontour (n / 3, input_tokens, name_,
                                      <bint> infinite_tensions_become_lines,
                                      &c, &info, &info_message, &bad_token_index)
      assert (info != 0 or c != NULL)
      if info == core.HOBBY_GUIDE_TOKEN_ERROR:
        # The bad token is actually at (3*bad_token_index), which
        # works fine, because tokens and their arguments take up 3
        # entries apiece. In fact, the main reason I did not simply
        # use a two-dimensional array, when I was writing Hobby guide
        # support for Sorts Mill Core Library, was so foreign function
        # interfaces would not differ too much due to column-major vs.
        # row-major ordering. -- Barry S.
        raise ValueError ({"info" : info,
                           "info_message" : to_str (info_message),
                           "bad_token_index" : bad_token_index})
      elif info != 0:
        raise RuntimeError ({"info" : info,
                             "info_message" : to_str (info_message)})
      ict = ImmutableContour ()
      ict.root[0] = c
      result = result_class (ict)
    elif (all (all (isinstance (x, numbers.Real) for x in subsequence))
          for subsequence in hobby_guide):
      #
      # The input is a token sequence broken into pieces
      # (subsequences).
      #
      token_sequence = [x for subsequence in hobby_guide for x in subsequence]
      try:
        result = result_class.fromHobbyGuide (token_sequence, name = name,
                                              infinite_tensions_become_lines \
                                              = infinite_tensions_become_lines)
      except ValueError as err:
        try:
          bad_token_index = err.args[0]["bad_token_index"]
        except:
          raise err
        raise_again_with_subsequence_indices (hobby_guide, err, bad_token_index)
    else:
      raise ValueError ("The input is not in a recognized form for a Hobby guide.")

    return result

  @classmethod
  def unitShape (result_class, int n = 0, name = None):
    # A Sorts Mill extension.
    #
    # FIXME: This docstring is poorly written.
    """Return a closed cubic contour, by default a unit circle.
  
    Takes an integer argument and returns a regular n-gon with that many
    sides. If the argument is positive the n-gon is inscribed on the
    unit circle, negative it is circumscribed, 0 the result is the unit
    circle, 1 a square. An argument value of 2, -1, or -2 is invalid.
  
    """
    if n == 2 or n == -1 or n == -2:
      raise ValueError (str (n) + " is not a valid argument.")
    cdef tools.ffcontour_t c = tools.make_unit_shape_ffcontour (n)
    cdef ImmutableContour ict = ImmutableContour (name = name)
    ict.root[0] = c
    return  result_class (ict)

#--------------------------------------------------------------------------

cdef class BaseMutableContour (BaseContour):

  """A parent class for mutable contours."""

  cdef void replace_from_ImmutableContour (self, ImmutableContour ic):
    raise NotImplementedError ()

  cpdef BaseMutableContour addExtrema (self, flag = None, ssize_t emsize = -1):
    # FIXME: This method needs better documentation.
    """Add extreme points to the contour."""
    cdef ImmutableContour ic = self.to_ImmutableContour ()
    ic = ic.addExtrema (flag, emsize)
    self.replace_from_ImmutableContour (ic)
    return self

  cpdef BaseMutableContour cluster (self, double within = -1.0, double max = -1.0):
    """Move clustered coordinates to a standard central value."""
    cdef ImmutableContour ic = self.to_ImmutableContour ()
    ic = ic.cluster (within, max)
    self.replace_from_ImmutableContour (ic)
    return self

  cpdef BaseMutableContour round (self, double factor = -1.0):
    """Round the x and y coordinates. If factor is specified then
    new-coord = round(factor*old-coord)/factor."""
    cdef ImmutableContour ic = self.to_ImmutableContour ()
    ic = ic.round (factor)
    self.replace_from_ImmutableContour (ic)
    return self

  cpdef BaseMutableContour simplify (self, double error_bound = -1.0,
                                     flags = (),
                                     double tan_bounds = -1.0,
                                     double linefixup = -1.0,
                                     double linelenmax = -1.0):
    # FIXME: This method needs better documentation.
    """Simplify the Contour."""
    cdef ImmutableContour ic = self.to_ImmutableContour ()
    ic = ic.simplify (error_bound, flags, tan_bounds, linefixup, linelenmax)
    self.replace_from_ImmutableContour (ic)
    return self

  cpdef BaseMutableContour transform (self, matrix, bint with_rounding = 1):
    # FIXME: Document with_rounding.
    """Transform the Contour, in place, by the PostScript-style
    transformation matrix (a 6-element sequence of numbers).

    """
    cdef ImmutableContour ic = self.to_ImmutableContour ()
    ic = ic.transform (matrix, with_rounding)
    self.replace_from_ImmutableContour (ic)
    return self

  cpdef BaseMutableContour interpolateOnCurvePoints (self):
    #
    # This is a Sorts Mill extension.
    #
    """Interpolate and make explicit the implicit on-curve Point objects
    in a quadratic Contour. Does nothing if called on a cubic Contour.

    """
    cdef ImmutableContour ic = self.to_ImmutableContour ()
    ic = ic.interpolateOnCurvePoints ()
    self.replace_from_ImmutableContour (ic)
    return self

#--------------------------------------------------------------------------

cdef class ImmutableContour (BaseContour):

  """A contour (usually a glyph outline) in the plane"""

  # An uncollectable Boehm GC node to serve as a garbage-collection
  # root (in lieu of a global variable). See Boehm GC documentation.
  cdef tools.ffcontour_t *root

  def __cinit__ (self):
    """Allocate the root node."""
    self.root = <tools.ffcontour_t *> \
                core.x_gc_malloc_uncollectable (sizeof (tools.ffcontour_t *))

  def __dealloc__ (self):
    """Deallocate the root node."""
    core.x_gc_free (<void *> self.root)

  cdef tools.ffcontour_t to_ffcontour (self):
    return self.root[0]

  cdef ImmutableContour to_ImmutableContour (self):
    return self

  def __init__ (self,
                is_quadratic = None, # Might be a contour or point sequence.
                is_closed = None,
                name = None,
                points = None,  # A sequence of points.
                spiros = None,
                closed = None):
    #
    # FIXME: This method needs MUCH better documentation.
    #
    """Construct a new ImmutableContour."""

    if spiros is not None:
      raise RuntimeError ("Spiro support currently is not implemented in Sorts Mill FFCompat.")

    check_precondition (closed is None or is_closed is None)
    if is_closed is None:
      is_closed = closed
    else:
      closed = is_closed

    cdef ImmutableContour ct
    cdef BaseContour bct
    cdef tools.ffcontour_t c
    cdef list lst
    cdef bint is_quadratic__
    cdef bint is_closed__
    cdef const char *name__
    cdef bytes b

    if isinstance (is_quadratic, ImmutableContour):
      # Duplicate an ImmutableContour.
      check_precondition (closed is None and
                          is_closed is None and
                          name is None and
                          points is None and
                          spiros is None)
      ct = is_quadratic
      self.root[0] = ct.root[0]
    elif isinstance (is_quadratic, BaseContour):
      check_precondition (closed is None and
                          is_closed is None and
                          name is None and
                          points is None and
                          spiros is None)
      bct = is_quadratic
      self.root[0] = Contour_to_ffcontour_t (bct)
    elif isinstance (is_quadratic, list):
      lst = is_quadratic
      self.root[0] = Point_list_to_ffcontour_t (NULL, lst)
    elif isinstance (is_quadratic, collections.Sequence):
      lst = list (is_quadratic)
      self.root[0] = Point_list_to_ffcontour_t (NULL, lst)
    else:
      is_quadratic__ = (0 if is_quadratic is None else is_quadratic)
      is_closed__ = (0 if is_closed is None else is_closed)
      if name is None:
        c = tools.make_ffcontour (is_quadratic__, is_closed__, NULL)
      else:
        b = to_bytes (name)
        c = tools.make_ffcontour (is_quadratic__, is_closed__, b)
      if points is not None:
        c = Point_list_to_ffcontour_t (c, [Point (x) for x in points])
      self.root[0] = c

  def __len__ (self):
    """How many points are contained in the ImmutableContour?

    """
    return tools.ffcontour_length (self.root[0])

  cpdef bint isEmpty (self):
    """Does the ImmutableContour contain no points?"""
    return <bint> tools.ffcontour_is_empty (self.root[0])

  @property
  def name (self):
    """The name of the ImmutableContour, or None if the ImmutableContour
    is nameless.

    """
    cdef const char *s = tools.ffcontour_name (self.root[0])
    if s == NULL:
      result = None
    else:
      result = to_str (s)
    return result

  @property
  def is_quadratic (self):
    """Is the ImmutableContour quadratic? (Otherwise it is cubic.)"""
    return <bint> tools.ffcontour_is_quadratic (self.root[0])

  @property
  def closed (self):
    """Is the ImmutableContour closed? (Otherwise it is open.)"""
    return <bint> tools.ffcontour_is_closed (self.root[0])

  @property
  def is_closed (self):
    """A synonym for the "closed" property."""
    return <bint> tools.ffcontour_is_closed (self.root[0])

  @property
  def points (self):
    """Return a deep copy of the points of the contour, as a list."""
    return ffcontour_t_to_ImmutablePoint_list (self.root[0])

  @property
  def spiros (self):
    raise RuntimeError ("Spiro support currently is not implemented in Sorts Mill FFCompat.")

  def __add__ (u, v):
    """Concatenation of points to those of a given ImmutableContour,
    returning a new ImmutableContour. The points to be concatenated
    may be specified in any of many ways.

    """

    cdef BasePoint bp
    cdef tools.ffpoint_t p
    cdef ImmutableContour u1
    cdef ImmutableContour v1
    cdef ImmutableContour c1 = ImmutableContour (u) # A copy of u.

    if isinstance (u, ImmutableContour):
      u1 = u
      if isinstance (v, BasePoint):
        bp = v
        p = tools.make_ffpoint (bp.x__, bp.y__, bp.on_curve__, bp.selected__)
        c1.root[0] = tools.ffcontour_push (u1.root[0], p)
      elif isinstance (v, ImmutableContour):
        v1 = v
        if tools.ffcontour_is_quadratic (u1.root[0]) != \
           tools.ffcontour_is_quadratic (v1.root[0]):
          raise ValueError ("Concatenating contours of different degrees.")
        c1.root[0] = tools.ffcontour_append (u1.root[0], v1.root[0])
      elif isinstance (v, BaseContour):
        if tools.ffcontour_is_quadratic (u1.root[0]) != v.is_quadratic:
          raise ValueError ("Concatenating contours of different degrees.")
        c1 += list (v)          # FIXME: Can this be sped up easily?
      elif isinstance (v, collections.Sequence):
        if len (v) == 0:
          pass
        elif isinstance (v[0], numbers.Number):
          c1 += ImmutablePoint (v)
        else:
          # FIXME: Can this be sped up easily?
          for x in v:
            c1 += ImmutablePoint (x)
      else:
        raise TypeError ("Invalid type.")
    else:
      raise NotImplemented ()
    return c1

  def __getitem__ (self, key):
    """Get an ImmutablePoint from the ImmutableContour, or get a slice of
    the ImmutableContour.

    """
    cdef ssize_t i
    cdef size_t n
    cdef tools.ffcontour_t c
    cdef tools.ffpoint_t p
    if isinstance (key, slice):
      result = ImmutableContour (is_quadratic = self.is_quadratic,
                                 is_closed = False,
                                 name = self.name,
                                 points = (tuple (self))[key])
    else:
      i = key
      c = self.root[0]
      n = tools.ffcontour_length (c)
      p = tools.ffcontour_ref (c, fold_index (n, i))
      result = ImmutablePoint (p.x, p.y, p.on_curve, p.selected)
    return result

  def get (self, key):
    """A synonym for __getitem__."""
    return self.__getitem__ (key)

  cpdef ImmutableContour set (self, key, value):
    # FIXME: This method needs better documentation.
    """Return a copy of the ImmutableContour with a point or slice set to
    new values. A slice can be specified in various ways.

    """
    cdef ssize_t i
    cdef size_t n
    cdef tools.ffpoint_t p
    cdef tools.ffcontour_t c
    cdef ImmutableContour result

    if isinstance (key, slice):
      # NOTE: This could be done with immutable vector operations, and
      # possibly could be faster if done that way, but then one would
      # have to duplicate the slice operations already implemented in
      # ‘list’. I do not feel like doing so.
      points = list (self)
      points[key] = list (value)
      result = ImmutableContour (is_quadratic = self.is_quadratic,
                                 is_closed = self.is_closed,
                                 name = self.name,
                                 points = points)
    else:
      if isinstance (value, BasePoint):
        pass
      elif isinstance (value, collections.Sequence):
        value = ImmutablePoint (value)
      else:
        raise TypeError ("Expected a BasePoint or sequence.")
      c = self.root[0]
      i = key
      n = tools.ffcontour_length (c)
      p = tools.make_ffpoint (value.x, value.y, value.on_curve, value.selected)
      result = ImmutableContour ()
      result.root[0] = tools.ffcontour_set (c, fold_index (n, i), p)
    return result

  def __iter__ (self):
    """Return an iterator that yields ImmutablePoints from the
    ImmutableContour, in order.

    """
    cdef tools.ffcontour_t c = self.root[0]
    cdef const tools.ffpoint_t *ptr
    cdef tools.ffpoint_t p
    cdef size_t i = 0
    ptr = tools.ffcontour_ptr (c, i)
    while ptr != NULL:
      p = ptr[0]
      yield ImmutablePoint (p.x, p.y, p.on_curve, p.selected)
      ptr = tools.ffcontour_next (c, i, ptr)
      i += 1

  def __reversed__ (self):
    """Return an iterator that yields ImmutablePoints from the
    ImmutableContour, in reverse order.

    """
    cdef tools.ffcontour_t c = self.root[0]
    cdef const tools.ffpoint_t *ptr
    cdef tools.ffpoint_t p
    cdef size_t n = tools.ffcontour_length (c)
    cdef size_t i
    if 0 < n:
      i = n
      ptr = tools.ffcontour_ptr (c, i - 1)
      while ptr != NULL:
        p = ptr[0]
        yield ImmutablePoint (p.x, p.y, p.on_curve, p.selected)
        ptr = tools.ffcontour_prev (c, i - 1, ptr)
        i -= 1

  cpdef ImmutableContour make_quadratic (self, bint is_quadratic = 1):
    """Return a copy of an ImmutableContour, possibly with a change of
    degree."""
    cdef ImmutableContour result
    cdef tools.ffcontour_t c
    cdef const char *error_message
    cdef bytes msg
    tools.ffcontour_make_quadratic (self.root[0], is_quadratic,
                                    &c, &error_message)
    if error_message == NULL:
      assert (c != NULL)
      result = ImmutableContour ()
      result.root[0] = c
    else:
      msg = error_message
      raise RuntimeError (to_str (msg))
    return result

  cpdef ImmutableContour make_cubic (self, bint is_cubic = 1):
    """Return a copy of an ImmutableContour, possibly with a change of
    degree."""
    cdef ImmutableContour result
    cdef tools.ffcontour_t c
    cdef const char *error_message
    cdef bytes msg
    tools.ffcontour_make_cubic (self.root[0], is_cubic,
                                &c, &error_message)
    if error_message == NULL:
      assert (c != NULL)
      result = ImmutableContour ()
      result.root[0] = c
    else:
      msg = error_message
      raise RuntimeError (to_str (msg))
    return result

  cpdef ImmutableContour make_closed (self, bint value = 1):
    """Return a copy of an ImmutableContour, possibly with a change as to
    whether the contour is open or closed."""
    cdef ImmutableContour result = ImmutableContour () 
    result.root[0] = tools.ffcontour_make_closed (self.root[0], value)
    return result

  cpdef ImmutableContour make_open (self, bint value = 1):
    """Return a copy of an ImmutableContour, possibly with a change as to
    whether the contour is open or closed."""
    cdef ImmutableContour result = ImmutableContour () 
    result.root[0] = tools.ffcontour_make_open (self.root[0], value)
    return result

  cpdef ImmutableContour set_name (self, name):
    """Return a copy of an ImmutableContour, with the name changed."""
    return ImmutableContour (is_quadratic = self.is_quadratic,
                             is_closed = self.is_closed,
                             name = name,
                             points = self.points,
                             spiros = None)
    
  cpdef ImmutableContour set_points (self, points):
    """Return a copy of an ImmutableContour, with the points changed."""
    return ImmutableContour (is_quadratic = self.is_quadratic,
                             is_closed = self.is_closed,
                             name = self.name,
                             points = points,
                             spiros = None)

  cpdef ImmutableContour clear (self):
    """Return a copy of the ImmutableContour, except empty of points."""
    return ImmutableContour (is_quadratic = self.is_quadratic,
                             is_closed = self.is_closed,
                             name = self.name,
                             points = [],
                             spiros = None)

  cpdef ImmutableContour append (self, point):
    """Append a point. The point may be specified by a sequence other than
    BasePoint (which will be passed to the ImmutablePoint
    constructor).

    """
    if not isinstance (point, BasePoint):
      point = ImmutablePoint (point)
    return self + point

  cpdef ImmutableContour insert (self, pos, point):
    """Insert a point in the manner of insertion into a list.  The point
    may be specified by a sequence (which will be passed to the
    ImmutablePoint constructor).

    """
    if not isinstance (point, BasePoint):
      point = ImmutablePoint (point)
    cdef list lst = list (self)
    lst.insert (pos, point)
    return self.set_points (lst)

  cpdef ImmutableContour moveTo (self, double x, double y):
    """Return a copy of the ImmutableContour but with only an on-curve
    point at (x,y).

    """
    cdef ImmutableContour result = ImmutableContour ()
    result.root[0] = tools.ffcontour_start_drawing (self.root[0], x, y)
    return result

  cpdef curveTo (self, p1, p2 = None, p3 = None, pos = None):
    # FIXME: This needs better documentation.
    """Return a copy of the ImmutableContour, with a line or spline added to it."""

    cdef ImmutableContour result = ImmutableContour ()
    cdef tools.ffcontour_t c = self.root[0]
    cdef size_t n = tools.ffcontour_length (c)
    cdef ssize_t i

    if n == 0:
      raise ValueError ("curveTo expects a non-empty ImmutableContour.")
    if pos is None:
      i = n - 1
    else:
      i = fold_index (n, pos)
    if not tools.ffcontour_ref (c, i).on_curve:
      i = tools.ffcontour_find_prev_on_curve (c, i)
      if i == -1:
        raise ValueError ("curveTo failed to find an on-curve point to start at.")
    assert (tools.ffcontour_ref (c, i).on_curve)

    if p2 is None:
      check_precondition (p3 is None)
      # Draw a line.
      result.root[0] = tools.ffcontour_draw_line (c, i, p1[0], p1[1])
    elif tools.ffcontour_is_quadratic (c):
      if p3 is not None:
        raise RuntimeError ("curveTo was passed three points but the ImmutableContour is quadratic.")
      result.root[0] = tools.ffcontour_draw_quadratic (c, i,
                                                     p1[0], p1[1],
                                                     p2[0], p2[1])
    else:
      if p3 is None:
        raise RuntimeError ("curveTo was passed two points but the ImmutableContour is cubic.")
      result.root[0] = tools.ffcontour_draw_cubic (c, i,
                                                 p1[0], p1[1],
                                                 p2[0], p2[1],
                                                 p3[0], p3[1])
    return result

  cpdef ImmutableContour lineTo (self, double x, double y, pos = None):
    # FIXME: This needs better documentation.
    """Add a straight line to the ImmutableContour.

    (NOTE: For historical reasons, this method does not handle a
    negative index the way Python users expect. It is better to use
    only positive indexes, or to use curveTo instead.)

    """
    cdef size_t n = tools.ffcontour_length (self.root[0])
    if pos is None or pos < 0 or n <= pos:
      pos = n - 1
    return self.curveTo ((x, y), pos = pos)

  cpdef ImmutableContour quadraticTo (self, cp, xy, pos = None):
    # FIXME: This needs better documentation.
    """Add a quadratic spline to the ImmutableContour.

    (NOTE: For historical reasons, this method does not handle a
    negative index the way Python users expect. It is better to use
    only positive indexes, or to use curveTo instead.)

    """
    if not tools.ffcontour_is_quadratic (self.root[0]):
      raise ValueError ("quadraticTo expects a quadratic ImmutableContour, but got a cubic one.")
    cdef size_t n = tools.ffcontour_length (self.root[0])
    if pos is None or pos < 0 or n <= pos:
      pos = n - 1
    return self.curveTo (cp, xy, pos = pos)

  cpdef ImmutableContour cubicTo (self, cp1, cp2, xy, pos = None):
    # FIXME: This needs better documentation.

    """Add a cubic spline to the ImmutableContour.

    (NOTE: For historical reasons, this method does not handle a
    negative index the way Python users expect. It is better to use
    only positive indexes, or to use curveTo instead.)

    """
    if tools.ffcontour_is_quadratic (self.root[0]):
      raise ValueError ("cubicTo expects a cubic Contour, but got a quadratic one.")
    cdef size_t n = tools.ffcontour_length (self.root[0])
    if pos is None or pos < 0 or n <= pos:
      pos = n - 1
    return self.curveTo (cp1, cp2, xy, pos = pos)

  cpdef ImmutableContour insertPoint (self, point, pos = None):
    """If the optional "pos" argument is given, a point will be inserted
    after the pos'th point. Otherwise, a point will be appended to the
    end of the contour. The point may be specified by a sequence
    (which will be passed to the point constructor).

    (NOTE: For historical reasons, this method does not handle a
    negative index the way Python users expect. It is better to use
    only positive indexes, or to use curveTo instead.)

    """
    if not isinstance (point, Point):
      point = Point (point)
    if pos is None:
      result = self + point
    else:
      lst = list (self)
      if pos < 0 or len (lst) <= pos:
        pos = len (lst) - 1
      lst.insert (pos + 1, point)
      result = self.set_points (lst)
    return result

  cpdef ImmutableContour makeFirst (self, ssize_t pos):
    """Rotate the points so that the pos'th point becomes the first. (In
    Sorts Mill FFCompat, makeFirst accepts negative indexes.)

    """
    cdef ImmutableContour result
    cdef size_t n = tools.ffcontour_length (self.root[0])
    if pos == 0:
      result = self
    else:
      if pos < 0:
        if n < -pos:
          raise IndexError ()
      else:
        if n <= pos:
          raise IndexError ()
      result = ImmutableContour ()
      result.root[0] = tools.ffcontour_rotate_left (self.root[0], pos)
    return result

  cpdef ImmutableContour rotateLeft (self, ssize_t n):
    """Rotate the points leftwards by a given amount. Negative numbers
    cause a rightwards rotation. Rotations by more than the length of
    the contour are allowed.

    """
    cdef ImmutableContour result
    if n == 0:
      result = self
    else:
      result = ImmutableContour ()
      result.root[0] = tools.ffcontour_rotate_left (self.root[0], n)
    return result

  cpdef ImmutableContour rotateRight (self, ssize_t n):
    """Rotate the points rightwards by a given amount. Negative numbers
    cause a leftwards rotation. Rotations by more than the length of
    the contour are allowed.

    """
    cdef ImmutableContour result
    if n == 0:
      result = self
    else:
      result = ImmutableContour ()
      result.root[0] = tools.ffcontour_rotate_right (self.root[0], n)
    return result

  cpdef ImmutableContour reverseDirection (self):
    """Reverse the direction of an open contour, or the orientation of a
    closed contour. For an open contour, this is done by reversing the
    order of points in the storage vector. For a closed contour, the
    first point in storage stays the same, whereas the other points
    are reversed in storage.

    """
    cdef ImmutableContour result = ImmutableContour ()
    result.root[0] = tools.ffcontour_reverse (self.root[0])
    return result

  cpdef ImmutableContour addExtrema (self, flag = None, ssize_t emsize = -1):
    # FIXME: This method needs better documentation.
    """Add extreme points to the contour."""
    cdef ImmutableContour result = ImmutableContour ()
    cdef tools.ffcontour_add_extrema_operation op
    cdef tools.ffcontour_t c
    cdef const char *error_message
    op = get_add_extrema_operation (flag)
    tools.ffcontour_add_extrema (self.root[0], op, emsize,
                                 &c, &error_message)
    if error_message != NULL:
      raise RuntimeError (to_str (error_message))
    result.root[0] = c
    return result

  cpdef ImmutableContour cluster (self, double within = -1.0, double max = -1.0):
    #
    # FIXME: THIS METHOD DOES NOT WORK.
    # FIXME: THIS METHOD DOES NOT WORK.
    # FIXME: THIS METHOD DOES NOT WORK.
    # FIXME: THIS METHOD DOES NOT WORK.
    # FIXME: THIS METHOD DOES NOT WORK.
    # FIXME: THIS METHOD DOES NOT WORK.
    # FIXME: THIS METHOD DOES NOT WORK.
    #
    # Specifically it is known to do segfaults both in Tools 3 and in
    # ‘FontForge’.
    #
    """Move clustered coordinates to a standard central value."""

    a = "a"
    if a == "a":    # Avoid an ‘Unreachable code’ warning from Cython.
      raise RuntimeError ("The " + (type(self)).__name__ +
                          ".cluster method currently is not working correctly.")

    cdef ImmutableContour result = ImmutableContour () 
    cdef tools.ffcontour_t c
    cdef const char *error_message
    tools.ffcontour_cluster (self.root[0], within, max, &c, &error_message)
    if error_message != NULL:
      raise RuntimeError (to_str (error_message))
    result.root[0] = c
    return result

  cpdef ImmutableContour round (self, double factor = -1.0):
    """Round the x and y coordinates. If factor is specified then
    new-coord = round(factor*old-coord)/factor."""
    cdef ImmutableContour result = ImmutableContour () 
    result.root[0] = tools.ffcontour_round (self.root[0], factor)
    return result

  def simplify (self, double error_bound = -1.0,
                flags = (),
                double tan_bounds = -1.0,
                double linefixup = -1.0,
                double linelenmax = -1.0):
    # FIXME: This method needs better documentation.
    """Simplify the contour."""
    if isinstance (flags, str) or isinstance (flags, bytes):
      # Allow a single flag in lieu of a sequence. This is a Sorts
      # Mill Tools extension.
      flags = (flags,)
    cdef ImmutableContour result = ImmutableContour ()
    cdef int operations = collect_simplification_flags (flags)
    cdef tools.ffcontour_t c
    cdef const char *error_message
    tools.ffcontour_simplify (self.root[0], error_bound, operations,
                              tan_bounds, linefixup, linelenmax, &c,
                              &error_message)
    if error_message != NULL:
      raise RuntimeError (to_str (error_message))
    result.root[0] = c
    return result

  cpdef ImmutableContour transformed_by (self, matrix, bint with_rounding = 1):
    """Return a copy of the Contour, transformed by the PostScript-style
    transformation matrix (a 6-element sequence of numbers).

    """
    cdef tools.ffpsmat_t a = tools.make_ffpsmat (matrix[0], matrix[1], matrix[2],
                                                 matrix[3], matrix[4], matrix[5])
    cdef ImmutableContour result = ImmutableContour ()
    if with_rounding:
      result.root[0] = tools.ffcontour_transform (self.root[0], a)
    else:
      result.root[0] = tools.ffcontour_transform_without_rounding (self.root[0], a)
    return result

  cpdef ImmutableContour transform (self, matrix, bint with_rounding = 1):
    """A synonym for transformed_by."""
    return self.transformed_by (matrix, with_rounding)

  def interpolateOnCurvePoints (self):
    """Interpolate and make explicit the implicit on-curve point objects
    in a quadratic contour. Does nothing if called on a cubic contour.

    """
    cdef ImmutableContour result = ImmutableContour ()
    cdef tools.ffcontour_t c = self.root[0]
    if tools.ffcontour_is_quadratic (c):
      result.root[0] = tools.ffcontour_interpolate_on_curve_points (c)
    return result

#--------------------------------------------------------------------------

cdef class Contour (BaseMutableContour):

  """A contour (usually a glyph outline) in the plane"""

  cdef bint is_quadratic__
  cdef bint is_closed__
  cdef object name__
  cdef list points__
  cdef object spiros__

  cdef tools.ffcontour_t to_ffcontour (self):
    return Contour_to_ffcontour_t (self)

  cdef void replace_from_ImmutableContour (self, ImmutableContour ic):
    cdef tools.ffcontour_t c = ic.root[0]
    cdef const char *nm = tools.ffcontour_name (c)
    self.is_quadratic__ = tools.ffcontour_is_quadratic (c)
    self.is_closed__ = tools.ffcontour_is_closed (c)
    self.name__ = (None if nm == NULL else to_str (nm))
    self.points__ = ffcontour_t_to_Point_list (c)
    self.spiros__ = None

  def __init__ (self,
                is_quadratic = None, # Might be a contour or point sequence.
                is_closed = None,
                name = None,
                points = None, # Might be a Contour or other sequence.
                spiros = None,
                closed = None):
    #
    # FIXME: This method needs MUCH better documentation.
    #
    """Construct a new Contour."""

    self.spiros__ = None

    if spiros is not None:
      raise RuntimeError ("Spiro support currently is not implemented in Sorts Mill FFCompat.")

    check_precondition (closed is None or is_closed is None)
    if is_closed is None:
      is_closed = closed
    else:
      closed = is_closed

    cdef bytes b
    cdef Contour ct
    cdef BaseContour bct
    cdef ImmutableContour ict
    cdef tools.ffcontour_t ffc

    if isinstance (is_quadratic, ImmutableContour):
      # Duplicate the contour data.
      check_precondition (closed is None and
                          is_closed is None and
                          name is None and
                          points is None and
                          spiros is None)
      ict = is_quadratic
      ffc = ict.root[0]
      self.is_quadratic__ = tools.ffcontour_is_quadratic (ffc)
      self.is_closed__ = tools.ffcontour_is_closed (ffc)
      self.name__ = to_str_or_None (tools.ffcontour_name (ffc))
      self.points__ = ffcontour_t_to_Point_list (ffc)
    elif isinstance (is_quadratic, BaseContour):
      # Duplicate the contour data.
      check_precondition (closed is None and
                          is_closed is None and
                          name is None and
                          points is None and
                          spiros is None)
      bct = is_quadratic
      self.is_quadratic__ = bct.is_quadratic
      self.is_closed__ = bct.is_closed
      self.name__ = bct.name
      self.points__ = bct.points # A shallow copy, if bct is a Contour.
    elif isinstance (is_quadratic, collections.Sequence):
      self.is_quadratic__ = False
      self.is_closed__ = False
      self.name__ = None
      if (isinstance (is_quadratic, list) and
          all (isinstance (x, Point) for x in is_quadratic)):
        self.points__ = points[:] # A shallow copy.
      else:
        self.points__ = [Point (x) for x in points]
    else:
      self.is_quadratic__ = (False if is_quadratic is None else is_quadratic)
      self.is_closed__ = (False if is_closed is None else is_closed)
      self.name__ = name
      self.points__ = ([] if points is None else [Point (x) for x in points])

  def __str__ (self):
    s = ("<Contour(quadratic)\n" if self.is_quadratic__ else "<Contour(cubic)\n")
    for p in self:
        s += "  ({},{}) ".format (p.x, p.y)
        s += ("on\n" if p.on_curve else "off\n")
    s += ">"
    return s

  def __len__ (self):
    """How many Point objects are contained in the Contour?"""
    return len (self.points__)

  cpdef bint isEmpty (self):
    """Does the Contour contain no Point objects?"""
    return <bint> (len (self.points__) == 0)

  @property
  def name (self):
    """The name of the Contour, or None."""
    return self.name__;

  @name.setter
  def name (self, value):
    if value is None:
      self.name__ = None
    else:
      self.name__ = to_str (value)

  @name.deleter
  def name (self):
    self.name__ = None

  @property
  def is_quadratic (self):
    """Is the Contour quadratic? (Otherwise it is cubic.)"""
    return <bint> self.is_quadratic__

  @is_quadratic.setter
  def is_quadratic (self, bint is_quad):
    cdef tools.ffcontour_t c1
    cdef tools.ffcontour_t c2
    cdef const char *error_message
    if is_quad != self.is_quadratic__:
      c1 = Contour_to_ffcontour_t (self)
      tools.ffcontour_make_quadratic (c1, is_quad, &c2, &error_message)
      if error_message == NULL:
        assert (c2 != NULL)
        self.points__ = ffcontour_t_to_Point_list (c2)
        self.is_quadratic__ = is_quad
      else:
        raise RuntimeError (to_str (error_message))

  @property
  def closed (self):
    """Is the Contour closed? (Otherwise it is open.)"""
    return <bint> self.is_closed__

  @property
  def is_closed (self):
    """A synonym for the "closed" attribute."""
    return <bint> self.is_closed__

  @closed.setter
  def closed (self, bint value):
    cdef tools.ffcontour_t c
    if value != self.is_closed__:
      c = Contour_to_ffcontour_t (self)
      c = tools.ffcontour_make_closed (c, value)
      self.points__ = ffcontour_t_to_Point_list (c)
      self.is_closed__ = value
    
  @is_closed.setter
  def is_closed (self, value):
    cdef tools.ffcontour_t c
    if value != self.is_closed__:
      c = Contour_to_ffcontour_t (self)
      c = tools.ffcontour_make_closed (c, value)
      self.points__ = ffcontour_t_to_Point_list (c)
      self.is_closed__ = value

  @property
  def points (self):
    """Return a shallow copy of the Contour instance's internal list of
    Point objects."""
    return self.points__[:] # The copy method does not exist in Python <3.3.

  @points.setter
  def points (self, value):
    self.points__ = [Point (x) for x in value]
    self.spiros__ = None

  @points.deleter
  def points (self):
    self.points__ = None
    self.spiros__ = None

  @property
  def spiros (self):
    raise NotImplementedError ("Spiro support currently is not implemented in Sorts Mill FFCompat.")

  @spiros.setter
  def spiros (self, value):
    raise NotImplementedError ("Spiro support currently is not implemented in Sorts Mill FFCompat.")

  def dup (self):
    """Return a deep copy of the Contour."""
    return Contour (self.is_quadratic__, self.is_closed__, self.name__,
                    [Point (x) for x in self.points__])

  def __iadd__ (self, other):
    """In-place concatenation of a point or points to the Contour."""
    cdef Contour ct = self + other
    self.points__ = ct.points__
    self.spiros__ = None
    return self

  def __add__ (u, v):
    """Concatenation of a point or points to those of a given Contour,
    returning a new Contour. The new Contour will have no name.

    """

    cdef Contour u1
    cdef Contour v1
    cdef ImmutableContour iv1
    cdef BaseContour bv1
    cdef list lst
    cdef Contour c1 = Contour (u) # A copy of u.
    c1.name__ = None

    if isinstance (u, Contour):
      u1 = u
      if isinstance (v, BasePoint):
        c1.points__.append (Point (v.x, v.y, v.on_curve, v.selected))
      elif isinstance (v, ImmutableContour):
        iv1 = v
        if u1.is_quadratic__ != <bint> tools.ffcontour_is_quadratic (iv1.root[0]):
          raise ValueError ("Concatenating contours of different degrees.")
        c1.points__ += ffcontour_t_to_Point_list (iv1.root[0])
      elif isinstance (v, BaseContour):
        bv1 = v
        if u1.is_quadratic__ != bv1.is_quadratic:
          raise ValueError ("Concatenating contours of different degrees.")
        c1.points__ += bv1.points # Preserves references if bv1 is a Contour.
      elif isinstance (v, collections.Sequence):
        if len (v) == 0:
          pass
        elif isinstance (v[0], numbers.Number):
          c1.points__.append (Point (v))
        else:
          if isinstance (v, list):
            lst = v
            if all (isinstance (x, Point) for x in lst):
              c1.points__ += lst # Preserves references.
            else:
              c1.points__ += [Point (x) for x in lst]
          else:
            c1.points__ += [Point (x) for x in v]
      else:
        raise TypeError ("Invalid type.")
    else:
      raise NotImplemented ()
    c1.spiros__ = None
    return c1

  def __getitem__ (self, key):
    # FIXME: Write better documentation.
    """Get a reference to a Point in the Contour, or make a new contour
    whose points are a slice of the original point list. Such a slice
    will contain references to (not copies of) the original points; it
    will be an open contour without a name.

    """
    cdef Contour ct
    if isinstance (key, slice):
      ct = Contour (self.is_quadratic__)
      ct.points__ = self.points__[key]
      result = ct
    else:
      result = self.points__[key]
    return result

  def __setitem__ (self, key, value):
    # FIXME: This method needs better documentation.
    """Set a Point in the Contour, or set a slice of the Contour."""
    cdef Contour ct
    if isinstance (key, slice):
      if isinstance (value, Contour):
        ct = value
        self.points__[key] = ct.points__
      else:
        self.points__[key] = [Point (x) for x in value]
    else:
      if isinstance (value, Point):
        self.points__[key] = value # Store a reference to the Point.
      else:
        self.points__[key] = Point (value)
    self.spiros__ = None

  def __delitem__ (self, key):
    # FIXME: ADD THIS TO ImmutableContour.
    """Delete a Point or slice from the Contour."""
    self.points__.__delitem__ (key)
    self.spiros__ = None

  def __iter__ (self):
    """Return an iterator that yields the Points in the Contour, in
    order.

    """
    return iter (self.points__)

  def __reversed__ (self):
    """Return an iterator that yields the Points in the Contour, in
    reverse order.

    """
    return reversed (self.points__)

  def clear (self):
    # This is a Sorts Mill FFCompat extension.
    """Clear the Contour of Points. Analogous to list.clear."""
    self.points__ = []         # Python <3.3 does not have list.clear.
    self.spiros__ = None
    
  def append (self, point):
    # This is a Sorts Mill FFCompat extension.
    """Append a Point. The Point may be specified by a sequence (which
    will be passed to the Point constructor).

    Analogous to list.append.

    """
    if not isinstance (point, Point):
      point = Point (point)
    self.points__.append (point)
    self.spiros__ = None

  def insert (self, pos, point):
    # This is a Sorts Mill FFCompat extension.
    """Insert a Point in the manner of insertion into a list.  The Point
    may be specified by a sequence (which will be passed to the Point
    constructor).

    Analogous to list.insert.

    """
    if not isinstance (point, Point):
      point = Point (point)
    self.points__.insert (pos, point)
    self.spiros__ = None

  cpdef Contour copy (self):
    # FIXME: If we add spiro support we should note that the copy has
    # spiros cleared.
    """Return a shallow copy. Analogous to list.copy."""
    return Contour (self.is_quadratic__, self.is_closed__, self.name__,
                    self.points__[:]) # Python <3.3 does not have list.copy.

  def count (self, x):
    # FIXME: ADD THIS TO ImmutableContour.
    """Return the total number of occurrences in the Contour of points
    "similar to" a given value x. Analogous to list.count.

    """
    return self.points__.count (x)

  def index (self, x, i = None, j = None):
    # FIXME: ADD THIS TO ImmutableContour.
    """Analogous to list.index."""
    if i is None:
      check_precondition (j is None)
      result = self.points__.index (x)
    elif j is None:
      result = self.points__.index (x, i)
    else:
      result = self.points__.index (x, i, j)
    return result

  def reverse (self):
    # FIXME: ADD REVERSAL OF THIS SORT TO ImmutableContour.
    """In-place reversal of the references to Point objects. Analogous to
    list.reverse.

    """
    self.points__.reverse ()
    self.spiros__ = None

  def extend (self, t):
    "c.extend(t) is a synonym for c+=t. Analogous to list.extend."""
    self += t

  def moveTo (self, double x, double y):
    """Clear the contour, then add an initial, on-curve Point at
    (x,y). (NOTE: In the original "fontforge" module, the Contour had
    to be empty before you called moveTo.)

    """
    self.points__ = [Point (x, y, True, False)]
    self.spiros__ = None
    return self                 # What FontForge does.

  def curveTo (self, p1, p2 = None, p3 = None, pos = None):
    #
    # FIXME: This needs better documentation.
    #
    # NOTE: This is a Sorts Mill extension.
    #
    """Add a line or spline to the Contour."""

    # NOTE: Only freshly created points are added to the contour;
    # never references to existing points.

    cdef Point pt1
    cdef Point pt2
    cdef Point pt3
    cdef list lst1
    cdef list lst2
    cdef ssize_t i
    cdef size_t n = len (self.points__)

    if n == 0:
      raise ValueError ("curveTo expects a non-empty Contour.")
    if pos is None:
      i = n - 1
    else:
      i = fold_index (n, pos)

    if not self.points__[i].on_curve:
      i1 = self.findPrevOnCurve (i)
      if i1 is None:
        raise ValueError ("curveTo failed to find an on-curve point to start at.")
      i = i1
    assert (self.points__[i].on_curve)

    def draw_cubic (p1, p2, p3):
      pass

    if p2 is None:              # Draw a line.
      check_precondition (p3 is None)
      pt1 = Point (p1[0], p1[1], True, False)
      if i == n:
        self.points__.append (pt1)
      else:
        lst1 = self.points__[:i + 1]
        lst2 = self.points__[i + 1:]
        self.points__ = lst1
        self.points__.append (pt1)
        self.points__ += lst2
    elif self.is_quadratic__:   # Draw a quadratic spline.
      if p3 is not None:
        raise RuntimeError ("curveTo was passed three points but the contour is quadratic.")
      pt1 = Point (p1[0], p1[1], False, False)
      pt2 = Point (p2[0], p2[1], True, False)
      if i == n:
        self.points__.append (pt1)
        self.points__.append (pt2)
      else:
        lst1 = self.points__[:i + 1]
        lst2 = self.points__[i + 1:]
        self.points__ = lst1
        self.points__.append (pt1)
        self.points__.append (pt2)
        self.points__ += lst2
    else:                       # Draw a cubic spline.
      if p3 is None:
        raise RuntimeError ("curveTo was passed two points but the contour is cubic.")
      pt1 = Point (p1[0], p1[1], False, False)
      pt2 = Point (p2[0], p2[1], False, False)
      pt3 = Point (p3[0], p3[1], True, False)
      if i == n:
        self.points__.append (pt1)
        self.points__.append (pt2)
        self.points__.append (pt3)
      else:
        lst1 = self.points__[:i + 1]
        lst2 = self.points__[i + 1:]
        self.points__ = lst1
        self.points__.append (pt1)
        self.points__.append (pt2)
        self.points__.append (pt3)
        self.points__ += lst2
        pass
    self.spiros__ = None
    return self              # What FontForge does in similar methods.

  cpdef Contour lineTo (self, double x, double y, pos = None):
    # FIXME: This needs better documentation.
    """Add a straight line to the Contour.

    (NOTE: For historical reasons, this method does not handle a
    negative index the way Python users expect. It is better to use
    only positive indexes, or to use curveTo instead.)

    """
    n = len (self.points__)
    if pos is None or pos < 0 or n <= pos:
      pos = n - 1
    return self.curveTo ((x, y), pos = pos)

  cpdef Contour quadraticTo (self, cp, xy, pos = None):
    # FIXME: This needs better documentation.
    """Add a quadratic spline to the Contour.

    (NOTE: For historical reasons, this method does not handle a
    negative index the way Python users expect. It is better to use
    only positive indexes, or to use curveTo instead.)

    """
    if not self.is_quadratic__:
      raise ValueError ("quadraticTo expects a quadratic Contour, but got a cubic one.")
    n = len (self.points__)
    if pos is None or pos < 0 or n <= pos:
      pos = n - 1
    return self.curveTo (cp, xy, pos = pos)

  cpdef Contour cubicTo (self, cp1, cp2, xy, pos = None):
    # FIXME: This needs better documentation.

    """Add a cubic spline to the Contour.

    (NOTE: For historical reasons, this method does not handle a
    negative index the way Python users expect. It is better to use
    only positive indexes, or to use curveTo instead.)

    """
    if self.is_quadratic__:
      raise ValueError ("cubicTo expects a cubic Contour, but got a quadratic one.")
    n = len (self.points__)
    if pos is None or pos < 0 or n <= pos:
      pos = n - 1
    return self.curveTo (cp1, cp2, xy, pos = pos)

  cpdef Contour insertPoint (self, point, pos = None):
    """If the optional "pos" argument is given, a point will be inserted
    after the pos'th point. Otherwise, a point will be appended to the
    end of the contour. The point may be specified by a sequence
    (which will be passed to the point constructor).

    (NOTE: For historical reasons, this method does not handle a
    negative index the way Python users expect. It is better to use
    only positive indexes, or to use curveTo instead.)

    """
    # If isinstance (point, Point), then what is inserted is a
    # reference to the existing point. Otherwise we insert a fresh
    # point.
    if not isinstance (point, Point):
      point = Point (point)
    if pos is None:
      self.points__.append (point)
    else:
      if pos < 0 or len (self.points__) <= pos:
        pos = len (self.points__) - 1
      self.points__.insert (pos + 1, point)
    return self

  cpdef Contour makeFirst (self, ssize_t pos):
    """Rotate the Points so that the pos'th Point becomes the first. (In
    Sorts Mill FFCompat, makeFirst accepts negative indexes.)

    """
    cdef size_t n = len (self.points__)
    if pos != 0:
      if pos < 0:
        if n < -pos:
          raise IndexError ()
      else:
        if n <= pos:
          raise IndexError ()
      self.rotateLeft (pos)
    return self

  cpdef Contour rotateLeft (self, ssize_t n):
    # This is a Sorts Mill extension. It is a generalization of
    # makeFirst.
    """Rotate the Points leftwards by a given amount. Negative numbers
    cause a rightwards rotation. Rotations by more than the length of
    the Point sequence are allowed.

    """
    cdef list lst1
    cdef list lst2
    cdef size_t npoints = len (self.points__)
    if n != 0:
      if n < 0:
        n = npoints - ((-n) % npoints);
      else:
        n %= npoints;
      lst1 = self.points__[:n]
      lst2 = self.points__[n:]
      self.points__ = lst2 + lst1
      self.spiros__ = None
    return self
  
  cpdef Contour rotateRight (self, ssize_t n):
    # This is a Sorts Mill extension.
    """Rotate the Points rightwards by a given amount. Negative numbers
    cause a leftwards rotation. Rotations by more than the length of
    the Point sequence are allowed.

    """
    return self.rotateLeft (-n)

  def reverseDirection (self):
    """Reverse the direction of an open Contour, or the orientation of a
    closed Contour. For an open Contour, this is done by reversing the
    order of Points in the storage vector. For a closed Contour, the
    first point in storage stays the same, whereas the other Points
    are reversed in storage.

    """
    cdef list sublist
    if 2 <= len (self.points__):
      if self.is_closed__:
        # Reverse around the zeroth point.
        sublist = self.points__[1:]
        sublist.reverse ()
        self.points__ = [self.points__[0]] + sublist
      else:
        # Just reverse the list.
        self.points__.reverse ()
      self.spiros__ = None
    return self

#--------------------------------------------------------------------------

cdef class BaseLayer:

  """A parent class for layers."""

  cdef tools.fflayer_t to_fflayer (self):
    cdef ImmutableLayer ct = ImmutableLayer (self)
    return ct.root[0]

  cdef ImmutableLayer to_ImmutableLayer (self):
    return ImmutableLayer (self)

  def __init__ (self, *args, **kwargs):
    raise NotImplementedError ()

  def __repr__ (self):
    return "{}(is_quadratic={}, contours={})" \
      .format (type(self).__name__, repr (self.is_quadratic), repr (self.contours))

  def __str__ (self):
    #
    # This definition of __str__ gives the same result as __repr__,
    # but with ‘structured’ whitespace.
    #

    def contour_str (BaseContour ct):
      cdef str s1
      cdef str t
      s1 = str (ct)
      if s1[-1] == "\n":
        s1 = s1[:-1]
      t = ""
      for c in s1:
        t += ("\n      " if c == "\n" else c)
      return t
      
    cdef BaseContour ct
    cdef str s
    cdef list contours = self.contours
    if len (contours) == 0:
      s = "[]"
    else:
      s = "[\n"
      for ct in contours[:-1]:
        s += "      " + contour_str (ct) + ",\n"
      s += "      " + contour_str (contours[-1]) + "\n   ]"
    return "{}(\n   is_quadratic={},\n   contours={}\n)\n" \
      .format (type(self).__name__, repr (self.is_quadratic), s)

  def __reduce__ (self):
    """Reduce the object for pickling."""
    return (self.__class__, (self.is_quadratic, self.contours))

  @property
  def is_quadratic (self):
    raise NotImplementedError ()

  @property
  def is_cubic (self):
    return (not self.is_quadratic)

  @property
  def contours (self):
    raise NotImplementedError ()

  def __getitem__ (self, key):
    return self.contours[key]

  def index (self, BaseContour ct, ssize_t start = 0, end = None):
    """Analogous to list.index."""
    cdef ssize_t end_
    if end is None:
      end_ = len (self)
    else:
      end_ = end
    cdef list lst = list (self)
    return lst.index (ct, start, end_)

  def count (self, BaseContour ct):
    """Analogous to list.count."""
    cdef list lst = list (self)
    return lst.count (ct)

  cpdef bint similar (self, other,
                      double point_error = -1.0,
                      double spline_error = -1.0):
    # FIXME: Check this documentation against actual behavior. In
    # FontForge, the HTML documentation does not match the
    # implementation!
    """Check whether this layer is similar to the other one, where
    the error arguments give the maximum distance (in em-units)
    allowed for contours to diverge.

    This is like the comparison operator, but that doesn't allow you
    to specify an error bound.

    """
    cdef const char *error_message1
    cdef const char *error_message2
    cdef int the_layers_are_similar
    cdef tools.fflayer_t ffy1 = self.to_fflayer ()
    cdef BaseLayer by
    if isinstance (other, BaseContour):
      by = (<BaseContour> other).to_ImmutableLayer ()
    elif isinstance (other, BaseLayer):
      by = other
    else:
      raise TypeError ()
    cdef tools.fflayer_t ffy2 = by.to_fflayer ()
    tools.fflayer_similarity (ffy1, ffy2, point_error, spline_error,
                              &the_layers_are_similar,
                              &error_message1, &error_message2)
    if error_message1 != NULL:
      raise ValueError('"self": ' + to_str (error_message1))
    if error_message2 != NULL:
      raise ValueError('"other": ' + to_str (error_message2))
    return <bint> the_layers_are_similar

  def __richcmp__ (y1, y2, int operation):
    cdef ImmutableLayer iy1
    cdef ImmutableLayer iy2
    cdef tools.fflayer_t y1_
    cdef tools.fflayer_t y2_
    cdef tools.ffcontour_t c1_
    cdef tools.ffcontour_t c2_
    cdef int cmp_value = 0
    cdef size_t i

    if isinstance (y1, BaseLayer):
      iy1 = (<BaseLayer> y1).to_ImmutableLayer ()
    elif isinstance (y1, BaseContour):
      iy1 = (<BaseContour> y1).to_ImmutableLayer ()
    else:
      raise TypeError ()

    if isinstance (y2, BaseLayer):
      iy2 = (<BaseLayer> y2).to_ImmutableLayer ()
    elif isinstance (y2, BaseContour):
      iy2 = (<BaseContour> y2).to_ImmutableLayer ()
    else:
      raise TypeError ()

    if not iy1.similar (iy2, 0.5, 1.0):
      # The following seems pretty silly to me, but it is roughly what
      # FontForge does. Perhaps FontForge should have raised
      # NotImplemented, instead.
      y1_ = iy1.root[0]
      y2_ = iy2.root[0]
      if isinstance (y1, Contour):
        cmp_value = 1;
      elif isinstance (y2, Contour):
        cmp_value = -1;
      elif tools.fflayer_length (y1_) < tools.fflayer_length (y2_):
        cmp_value = -1;
      elif tools.fflayer_length (y2_) < tools.fflayer_length (y1_):
        cmp_value = 1;
      else:
        cmp_value = 0
        j = 0
        while cmp_value == 0 and i < tools.fflayer_length (y1_):
          c1_ = tools.fflayer_ref (y1_, j)
          c2_ = tools.fflayer_ref (y2_, j)
          if tools.ffcontour_length (c1_) < tools.ffcontour_length (c2_):
            cmp_value = -1
          elif tools.ffcontour_length (c2_) < tools.ffcontour_length (c1_):
            cmp_value = 1
          else:
            i = 0
            cmp_value = 0
            while cmp_value == 0 and i < tools.ffcontour_length (c1_):
              cmp_value = tools.ffpoint_compare (tools.ffcontour_ref (c1_, i),
                                                 tools.ffcontour_ref (c2_, i))
              i += 1
              j += 1
        if tools.fflayer_length (y1_) <= j:
          cmp_value = -1

    return cmp_to_richcmp (cmp_value, operation)

#--------------------------------------------------------------------------

cdef class ImmutableLayer (BaseLayer):

  cdef tools.fflayer_t to_fflayer (self):
    return self.root[0]

  cdef ImmutableLayer to_ImmutableLayer (self):
    return self

  # An uncollectable Boehm GC node to serve as a garbage-collection
  # root (in lieu of a global variable). See Boehm GC documentation.
  cdef tools.fflayer_t *root

  def __cinit__ (self):
    """Allocate the root node."""
    self.root = <tools.fflayer_t *> \
                core.x_gc_malloc_uncollectable (sizeof (tools.fflayer_t *))

  def __dealloc__ (self):
    """Deallocate the root node."""
    core.x_gc_free (<void *> self.root)

  def __init__ (self, bint is_quadratic = 0, contours = None):
    """Make a new layer, optionally initializing it with some contours."""
    cdef tools.ffcontour_ivect_t vec
    cdef ImmutableContour ct
    cdef ImmutableLayer y
    cdef BaseContour bct
    cdef BaseLayer by
    if contours is None:
      self.root[0] = tools.make_fflayer (is_quadratic, NULL)
    elif isinstance (is_quadratic, ImmutableLayer):
      # Using __init__ to create a copy of an ImmutableLayer.
      assert (contours is None)
      y = is_quadratic
      self.root[0] = y.root[0]
    elif isinstance (is_quadratic, BaseLayer):
      # Using __init__ to convert a BaseLayer to an ImmutableLayer.
      by = is_quadratic
      vec = NULL
      for bct in by.contours:
        ct = bct.to_ImmutableContour ()
        vec = tools.ffcontour_ivect_push (vec, ct.root[0])
      self.root[0] = tools.make_fflayer (by.is_quadratic, vec)
    else:
      vec = NULL
      for bct in contours:
        ct = bct.to_ImmutableContour ()
        if <bint> tools.ffcontour_is_quadratic (ct.root[0]) != is_quadratic:
          raise ValueError ("A contour degree does not match the layer degree.")
        vec = tools.ffcontour_ivect_push (vec, ct.root[0])
      self.root[0] = tools.make_fflayer (is_quadratic, vec)

  @property
  def is_quadratic (self):
    return <bint> tools.fflayer_is_quadratic (self.root[0])

  @property
  def contours (self):
    return fflayer_t_to_ImmutableContour_list (self.root[0])

  def __len__ (self):
    return tools.fflayer_length (self.root[0])

  cpdef bint isEmpty (self):
    """Is the ImmutableLayer empty?"""
    return <bint> tools.fflayer_is_empty (self.root[0])

  cpdef get (self, key):
    """Return an item or slice from an ImmutableLayer."""
    cdef size_t n
    cdef ssize_t i
    cdef ImmutableContour ct
    if isinstance (key, slice):
      result = ImmutableLayer (tools.fflayer_is_quadratic (self.root[0]),
                               list (self)[key])
    else:
      n = tools.fflayer_length (self.root[0])
      i = fold_index (n, key)
      ct = ImmutableContour ()
      ct.root[0] = tools.fflayer_ref (self.root[0], i)
      result = ct
    return result

  def __getitem__ (self, key):
    return self.get (key)

  cpdef ImmutableLayer set (self, key, value):
    """Return a new ImmutableLayer with a change to the value of an
    item or a slice."""
    cdef list lst
    cdef size_t n
    cdef ssize_t i
    cdef BaseContour bct
    cdef ImmutableContour ct
    cdef ImmutableLayer result
    cdef tools.fflayer_t y
    if isinstance (key, slice):
      lst = list (self)
      lst[key] = value
      result = ImmutableLayer (tools.fflayer_is_quadratic (self.root[0]), lst)
    else:
      n = tools.fflayer_length (self.root[0])
      i = fold_index (n, key)
      bct = value
      ct = bct.to_ImmutableContour ()
      y = tools.fflayer_set (self.root[0], i, ct.root[0])
      result = ImmutableLayer ()
      result.root[0] = y
    return result

  def __iter__ (self):
    """Return an iterator that yields the layer's contours, in order."""
    cdef tools.fflayer_t y = self.root[0]
    cdef const tools.ffcontour_t *ptr
    cdef tools.ffcontour_t c
    cdef ImmutableContour ct
    cdef size_t i = 0
    ptr = tools.fflayer_ptr (y, i)
    while ptr != NULL:
      c = ptr[0]
      ct = ImmutableContour ()
      ct.root[0] = c
      yield ct
      ptr = tools.fflayer_next (y, i, ptr)
      i += 1

  def __reversed__ (self):
    """Return an iterator that yields the layer's contours, in reverse
    order.

    """
    cdef tools.fflayer_t y = self.root[0]
    cdef const tools.ffcontour_t *ptr
    cdef tools.ffcontour_t c
    cdef ImmutableContour ct
    cdef size_t n = tools.fflayer_length (y)
    cdef size_t i
    if 0 < n:
      i = n
      ptr = tools.fflayer_ptr (y, i - 1)
      while ptr != NULL:
        c = ptr[0]
        ct = ImmutableContour ()
        ct.root[0] = c
        yield ct
        ptr = tools.fflayer_prev (y, i - 1, ptr)
        i -= 1

  def __add__ (u, v):
    cdef ImmutableLayer y0
    cdef ImmutableLayer y1
    cdef ImmutableLayer y2
    if isinstance (u, ImmutableLayer):
      if isinstance (v, BaseContour):
        v = [v]
      y0 = u
      y1 = (v.to_ImmutableLayer () if isinstance (v, BaseLayer)
            else ImmutableLayer (tools.fflayer_is_quadratic (y0.root[0]), v))
      if (<bint> tools.fflayer_is_quadratic (y0.root[0])
          != <bint> tools.fflayer_is_quadratic (y1.root[0])):
        raise ValueError ("The layer degrees do not match.")
      y2 = ImmutableLayer ()
      y2.root[0] = tools.fflayer_append (y0.root[0], y1.root[0])
      result = y2
    else:
      # Assume u is something to which a list can be concatenated.
      result = u + list (v)
    return result

  cpdef ImmutableLayer append (ImmutableLayer self, BaseContour ct):
    """Append a contour."""
    return self + [ct]

  cpdef ImmutableLayer extend (ImmutableLayer self, contours):
    """Extend by appending contours."""
    return self + contours

  cpdef ImmutableLayer insert (ImmutableLayer self, i, BaseContour ct):
    """Analogous to list.insert, though returning a new ImmutableLayer."""
    cdef list lst = list (self)
    lst.insert (i, ct)
    return ImmutableLayer (tools.fflayer_is_quadratic (self.root[0]), lst)

  cpdef ImmutableLayer remove (ImmutableLayer self, BaseContour ct):
    """Analogous to list.remove, though returning a new ImmutableLayer."""
    cdef list lst = list (self)
    lst.remove (ct)
    return ImmutableLayer (tools.fflayer_is_quadratic (self.root[0]), lst)

  cpdef ImmutableLayer clear (ImmutableLayer self):
    """Return an empty layer of the same degree."""
    return ImmutableLayer (tools.fflayer_is_quadratic (self.root[0]))

  cpdef ImmutableLayer sort (ImmutableLayer self, key = None, reverse = False):
    """Analogous to list.sort, though returning a new ImmutableLayer."""
    cdef list lst = list (self)
    lst.sort (key = key, reverse = reverse)
    return ImmutableLayer (tools.fflayer_is_quadratic (self.root[0]), lst)

  cpdef ImmutableLayer reverse (ImmutableLayer self):
     """Analogous to list.reverse, though returning a new
     ImmutableLayer."""
     # This definition will work for Layer.reverse without being
     # overridden.
     cdef ImmutableLayer y = ImmutableLayer ()
     y.root[0] = tools.fflayer_reverse (self.root[0])
     return y

  cpdef ImmutableLayer addExtrema (ImmutableLayer self, flag = None, size_t emsize = -1):
    # FIXME: This method needs better documentation.
    """Add extreme points to the layer."""
    cdef tools.ffcontour_add_extrema_operation op
    cdef tools.fflayer_t y
    cdef const char *error_message
    op = get_add_extrema_operation (flag)
    tools.fflayer_add_extrema (self.root[0], op, emsize,
                               &y, &error_message)
    if error_message != NULL:
      raise RuntimeError (to_str (error_message))
    cdef ImmutableLayer result = ImmutableLayer ()
    result.root[0] = y
    return result

  cpdef ImmutableLayer cluster (ImmutableLayer self, double within = -1.0, double max = -1.0):
    #
    # FIXME: THIS METHOD DOES NOT WORK.
    # FIXME: THIS METHOD DOES NOT WORK.
    # FIXME: THIS METHOD DOES NOT WORK.
    # FIXME: THIS METHOD DOES NOT WORK.
    # FIXME: THIS METHOD DOES NOT WORK.
    # FIXME: THIS METHOD DOES NOT WORK.
    # FIXME: THIS METHOD DOES NOT WORK.
    #
    # Specifically, Contour.cluster is known to do segfaults both in
    # Tools 3 and in ‘FontForge’.
    #
    # This method does not need to be overridden in the Layer
    # subclass.
    """Move clustered coordinates to a standard central value."""

    a = "a"
    if a == "a":    # Avoid an ‘Unreachable code’ warning from Cython.
      raise RuntimeError ("The ImmutableLayer.cluster method currently is not working correctly.")

    cdef tools.fflayer_t y
    cdef const char *error_message
    tools.fflayer_cluster (self.root[0], within, max, &y, &error_message)
    if error_message != NULL:
      raise RuntimeError (to_str (error_message))
    cdef ImmutableLayer result = ImmutableLayer ()
    result.root[0] = y
    return result

  cpdef ImmutableLayer round (ImmutableLayer self, double factor = -1.0):
    """Round the x and y coordinates. If factor is specified then
    new-coord = round(factor*old-coord)/factor."""
    cdef ImmutableLayer result = ImmutableLayer ()
    result.root[0] = tools.fflayer_round (self.root[0], factor)
    return result

  cpdef ImmutableLayer simplify (ImmutableLayer self,
                                 double error_bound = -1.0,
                                 flags = (),
                                 double tan_bounds = -1.0,
                                 double linefixup = -1.0,
                                 double linelenmax = -1.0):
    # FIXME: This method needs better documentation.
    """Simplify the layer."""
    if isinstance (flags, str) or isinstance (flags, bytes):
      # Allow a single flag in lieu of a sequence. This is a Sorts
      # Mill Tools extension.
      flags = (flags,)
    cdef int operations = collect_simplification_flags (flags)
    cdef tools.fflayer_t y
    cdef const char *error_message
    tools.fflayer_simplify (self.root[0], error_bound, operations,
                            tan_bounds, linefixup, linelenmax, &y,
                            &error_message)
    if error_message != NULL:
      raise RuntimeError (to_str (error_message))
    cdef ImmutableLayer result = ImmutableLayer ()
    result.root[0] = y
    return result

#--------------------------------------------------------------------------

######--------------------------------------------------------------------------
#####
#####cdef str layer_wrong_degree_msg ():
#####  return "Attempt to store a Contour of the wrong degree in a Layer."
#####
#####cdef class Layer (Layer):
#####
#####  """An ordered collection of Contour objects of the same degree."""
#####
#####  cdef bint is_quad
#####
#####  cdef inline bint check_degrees (Layer self) except 0:
#####    cdef bint is_quad = self.is_quad
#####    cdef tools.fflayer_t y = self.root[0]
#####    cdef const tools.ffcontour_t *ptr
#####    cdef size_t i = 0
#####    ptr = tools.fflayer_ptr (y, i)
#####    while ptr != NULL:
#####      if (<bint> tools.ffcontour_is_quadratic (ptr[0])) != is_quad:
#####        raise ValueError (layer_wrong_degree_msg ())
#####      ptr = tools.fflayer_next (y, i, ptr)
#####      i += 1
#####    return 1
#####
#####  def __init__ (self, contours = None, is_quadratic = None):
#####    cdef Layer other_layer
#####    cdef bint is_quad
#####    if isinstance (contours, Layer):
#####      # Let the ‘is_quadratic’ argument be redundantly
#####      # specified. Specifying it actually serves as a check, so is
#####      # useful.
#####      other_layer = contours
#####      if (is_quadratic is not None and (<bint> is_quadratic) != other_layer.is_quad):
#####        raise ValueError ("The arguments to the Layer constructor disagree on the degree.")
#####      self.is_quad = other_layer.is_quad
#####      self.root[0] = other_layer.root[0]
#####    elif isinstance (contours, Layer) or isinstance (contours, collections.Sequence):
#####      self.is_quad = (0 if is_quadratic is None else (<bint> is_quadratic))
#####      Layer.__init__ (self, contours)
#####      self.check_degrees ()
#####    else:
#####      if is_quadratic is None:
#####        # For compatibility with FontForge.
#####        is_quad = contours
#####        self.is_quad = is_quad
#####        self.root[0] = NULL
#####      else:
#####        self.is_quad = is_quadratic
#####        self.root[0] = NULL
#####
#####  def __repr__ (self):
#####    return (type(self).__name__ +
#####            "(contours=" + repr (list (self)) +
#####            ", is_quadratic=" + repr (self.is_quad) + ")")
#####
#####  def __getitem__ (self, key):
#####    cdef size_t n
#####    cdef ssize_t i
#####    cdef Contour ct
#####    if isinstance (key, slice):
#####      result = Layer (list (self)[key], self.is_quad)
#####    else:
#####      n = tools.fflayer_length (self.root[0])
#####      i = fold_index (n, key)
#####      ct = Contour ()
#####      ct.root[0] = tools.fflayer_ref (self.root[0], i)
#####      result = ct
#####    return result
#####
#####  def __setitem__ (self, key, value):
#####    cdef list lst
#####    cdef Layer clst
#####    cdef tools.fflayer_t y
#####    cdef size_t n
#####    cdef ssize_t i
#####    cdef Contour ct
#####    cdef tools.ffcontour_t c
#####    if isinstance (key, slice):
#####      lst = list (self)
#####      lst[key] = value
#####      clst = Layer (lst)
#####      y = clst.root[0]
#####      if self.is_quad:
#####        if not (<bint> tools.fflayer_is_quadratic (y)):
#####          raise ValueError (layer_wrong_degree_msg ())
#####      else:
#####        if not (<bint> tools.fflayer_is_cubic (y)):
#####          raise ValueError (layer_wrong_degree_msg ())
#####      self.root[0] = y
#####    else:
#####      n = tools.fflayer_length (self.root[0])
#####      i = fold_index (n, key)
#####      ct = value
#####      c = ct.root[0]
#####      if self.is_quad != <bint> tools.ffcontour_is_quadratic (c):
#####        raise ValueError (layer_wrong_degree_msg ())
#####      self.root[0] = tools.fflayer_set (self.root[0], i, c)
#####
#####  def __add__ (u, v):
#####    cdef Layer y1
#####    cdef Layer y2
#####    if isinstance (u, Layer):
#####      y1 = u
#####      result = Layer (Layer.__add__ (y1, v), y1.is_quad)
#####    elif isinstance (u, Layer):
#####      y2 = u
#####      result = Layer.__add__ (y2, v)
#####    else:
#####      # Assume u is something to which a list can be concatenated.
#####      result = u + list (v)
#####    return result
#####
#####  def __iadd__ (Layer self, other):
#####    cdef Layer y = self + other
#####    self.root[0] = y.root[0]
#####    return self
#####
#####  cpdef Layer insert (Layer self, i, Contour ct):
#####    """Analogous to list.insert."""
#####    cdef list lst = list (self)
#####    lst.insert (i, ct)
#####    cdef Layer result = Layer (lst, self.is_quad)
#####    self.root[0] = result.root[0]
#####    return self
#####
#####  @property
#####  def is_quadratic (self):
#####    """Is the Layer quadratic?"""
#####    return self.is_quad
#####
#####  @is_quadratic.setter
#####  def is_quadratic (self, bint is_quad):
#####    cdef Layer lst
#####    if self.is_quad != is_quad:
#####      lst = Layer (self)
#####      lst.is_quadratic = is_quad
#####      self.root[0] = lst.root[0]
#####    self.is_quad = is_quad
#####
#####  @property
#####  def is_cubic (self):
#####    """Is the Layer cubic?"""
#####    return self.is_quad
#####
#####  @is_cubic.setter
#####  def is_cubic (self, bint is_cubic):
#####    if self.is_quad != is_cubic:
#####      (<Layer> self).is_cubic = is_cubic
#####    self.is_quad = not is_cubic
#####
#####  cpdef bint isMixedDegree (self):
#####    """Does the Layer contain a mixture of quadratic and cubic Contour
#####    objects? Always returns False.
#####
#####    """
#####    return 0
#####
#####  cpdef Layer dup (Layer self):
#####    """Return a deep copy of a Layer. (Internally, a Layer is mostly
#####    immutable objects, and we copy only what is not immutable.)"""
#####    cdef Layer ylst = Layer (None, self.is_quad)
#####    ylst.root[0] = self.root[0]
#####    return self
#####
#--------------------------------------------------------------------------

def unitShape (int n = 0):
  # FIXME: This docstring is poorly written.
  """Return a closed cubic contour, by default a unit circle.

  Takes an integer argument and returns a regular n-gon with that many
  sides. If the argument is positive the n-gon is inscribed on the
  unit circle, negative it is circumscribed, 0 the result is the unit
  circle, 1 a square. An argument value of 2, -1, or -2 is invalid.

  (This function also is available as a class method of subclasses of
  BaseContour, including Contour. It is included as a separate
  function for compatibility with FontForge.)

  """
  return Contour.unitShape (n)

#--------------------------------------------------------------------------
#
# Module initialization.
#

# NOTE: If scm_init_guile() exits the program with an error message,
# because it does not work on your platform, you can comment out the
# call here and use a special Python interpreter that initializes
# Guile. Such an interpreter is built by Sorts Mill Core Guile when it
# --with-python has been specified during its configuration.
#
# FIXME: Write a configuration test for whether scm_init_guile()
# works.
#
guile.scm_init_guile ()

# Aliases for classes under their traditional FontForge names.
this_module.point = Point
this_module.contour = Contour
#this_module.layer = Layer
#this_module.glyphPen = GlyphPen
#this_module.glyph = Glyph
#this_module.selection = Selection
#this_module.math = Math
#this_module.font = Font

#--------------------------------------------------------------------------
