# -*- coding: utf-8 -*-

# Copyright (C) 2017 Khaled Hosny and Barry Schwartz
#
# This file is part of the Sorts Mill FFCompat.
# 
# Sorts Mill FFCompat is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
# 
# Sorts Mill FFCompat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.

cdef extern from "<sortsmill/core.h>" nogil:

  #----------------------------------------------------------------------

  void *x_gc_malloc (size_t sz)
  void *x_gc_malloc_atomic (size_t sz)
  void *x_gc_malloc_uncollectable (size_t sz)
  void x_gc_free (void *p)

  #----------------------------------------------------------------------

  ctypedef struct f64ivect_atomic_t__vector__:
    pass
  ctypedef const f64ivect_atomic_t__vector__ *f64ivect_atomic_t

  size_t ffpoint_ivect_length (f64ivect_atomic_t);
  double ffpoint_ivect_ref (f64ivect_atomic_t, size_t);
  const double *ffpoint_ivect_ptr (f64ivect_atomic_t, size_t);
  const double *ffpoint_ivect_next (f64ivect_atomic_t, size_t, const double *);
  const double *ffpoint_ivect_prev (f64ivect_atomic_t, size_t, const double *);
  void ffpoint_ivect_refs (f64ivect_atomic_t, size_t i, size_t n, double array[]);
  f64ivect_atomic_t ffpoint_ivect_set (f64ivect_atomic_t, size_t, double);
  f64ivect_atomic_t ffpoint_ivect_sets (f64ivect_atomic_t, size_t i, size_t n, double array[]);
  f64ivect_atomic_t ffpoint_ivect_push (f64ivect_atomic_t, double);
  f64ivect_atomic_t ffpoint_ivect_pushes (f64ivect_atomic_t, size_t, double array[]);
  f64ivect_atomic_t ffpoint_ivect_pop (f64ivect_atomic_t);
  f64ivect_atomic_t ffpoint_ivect_pops (f64ivect_atomic_t, size_t);
  f64ivect_atomic_t ffpoint_ivect_slice (f64ivect_atomic_t, size_t start, size_t end);
  f64ivect_atomic_t ffpoint_ivect_append (f64ivect_atomic_t, f64ivect_atomic_t);

  #----------------------------------------------------------------------

  enum hobby_guide_token:
    HOBBY_GUIDE_POINT = 0
    HOBBY_GUIDE_CYCLE = 1
    HOBBY_GUIDE_TENSION = 2
    HOBBY_GUIDE_ATLEAST = 3
    HOBBY_GUIDE_DIR = 4
    HOBBY_GUIDE_CURL = 5
    HOBBY_GUIDE_CTRL = 6

  enum:
    HOBBY_GUIDE_TOKEN_ERROR = -1
    HOBBY_GUIDE_SOLUTION_NOT_FOUND = -2

  void solve_hobby_guide_tokens (size_t n_input,
                                 const double input_tokens[],
                                 size_t *n_output,
                                 double output_tokens[],
                                 int *info, const char **info_message,
                                 size_t *bad_token_index);

  #----------------------------------------------------------------------
