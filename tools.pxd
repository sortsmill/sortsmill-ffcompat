# -*- coding: utf-8 -*-

# Copyright (C) 2017 Khaled Hosny and Barry Schwartz
#
# This file is part of the Sorts Mill FFCompat.
# 
# Sorts Mill FFCompat is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
# 
# Sorts Mill FFCompat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.

from types cimport _Bool
from libc.stdint cimport uint8_t

cdef extern from "<sortsmill/tools-api.h>" nogil:

  # --------------------------------------------------------------

  _Bool get_no_windowing_ui()
  void set_no_windowing_ui(_Bool)

  _Bool get_running_script()
  void set_running_script(_Bool)

  # --------------------------------------------------------------

  ctypedef struct ttf_instructions:
    size_t num_instructions
    uint8_t *instructions
    char *error_message
    size_t error_offset

  ttf_instructions *parse_ttf_instructions (const char *)
  char *unparse_ttf_instructions (const ttf_instructions *)

  # --------------------------------------------------------------

  ctypedef struct ffpsmat:
    double a[6]

  ctypedef const ffpsmat *ffpsmat_t

  ffpsmat_t make_ffpsmat (double a0, double a1, double a2,
                          double a3, double a4, double a5)
  const char *ffpsmat_to_string (ffpsmat_t);

  # --------------------------------------------------------------

  ctypedef struct ffpoint__:
    double x
    double y
    _Bool on_curve
    _Bool selected

  ctypedef const ffpoint__ *ffpoint_t

  ctypedef struct ffpoint_ivect_t__vector__:
    pass
  ctypedef const ffpoint_ivect_t__vector__ *ffpoint_ivect_t

  ffpoint_t make_ffpoint (double x, double y, _Bool on_curve, _Bool selected);
  ffpoint_t ffpoint_transform (ffpoint_t, ffpsmat_t);
  ffpoint_t ffpoint_transform_without_rounding (ffpoint_t, const ffpsmat_t);
  int ffpoint_compare (ffpoint_t, ffpoint_t);
  int ffpoint_compare_nearness (ffpoint_t, ffpoint_t, _Bool (*)(double, double));
  const char *ffpoint_to_string (ffpoint_t);

  size_t ffpoint_ivect_length (ffpoint_ivect_t);
  ffpoint_t ffpoint_ivect_ref (ffpoint_ivect_t, size_t);
  const ffpoint_t *ffpoint_ivect_ptr (ffpoint_ivect_t, size_t);
  const ffpoint_t *ffpoint_ivect_next (ffpoint_ivect_t, size_t, const ffpoint_t *);
  const ffpoint_t *ffpoint_ivect_prev (ffpoint_ivect_t, size_t, const ffpoint_t *);
  void ffpoint_ivect_refs (ffpoint_ivect_t, size_t i, size_t n, ffpoint_t array[]);
  ffpoint_ivect_t ffpoint_ivect_set (ffpoint_ivect_t, size_t, ffpoint_t);
  ffpoint_ivect_t ffpoint_ivect_sets (ffpoint_ivect_t, size_t i, size_t n, ffpoint_t array[]);
  ffpoint_ivect_t ffpoint_ivect_push (ffpoint_ivect_t, ffpoint_t);
  ffpoint_ivect_t ffpoint_ivect_pushes (ffpoint_ivect_t, size_t, ffpoint_t array[]);
  ffpoint_ivect_t ffpoint_ivect_pop (ffpoint_ivect_t);
  ffpoint_ivect_t ffpoint_ivect_pops (ffpoint_ivect_t, size_t);
  ffpoint_ivect_t ffpoint_ivect_slice (ffpoint_ivect_t, size_t start, size_t end);
  ffpoint_ivect_t ffpoint_ivect_append (ffpoint_ivect_t, ffpoint_ivect_t);

  # --------------------------------------------------------------

  ctypedef struct ffspiropoint__:
    double x
    double y
    char ty
    _Bool selected

  ctypedef const ffspiropoint__ *ffspiropoint_t

  ctypedef struct ffspiropoint_ivect_t__vector__:
    pass
  ctypedef const ffspiropoint_ivect_t__vector__ *ffspiropoint_ivect_t

  _Bool allowed_ffspiropoint_ty (char ty);
  ffspiropoint_t make_ffspiropoint (double x, double y, char ty, _Bool selected);
  ffspiropoint_t ffspiropoint_transform (ffspiropoint_t, const ffpsmat_t);
  ffspiropoint_t ffspiropoint_transform_without_rounding (ffspiropoint_t, ffpsmat_t);
  int ffspiropoint_compare (ffspiropoint_t, ffspiropoint_t);
  int ffspiropoint_compare_nearness (ffspiropoint_t, ffspiropoint_t,
                                     _Bool (*)(double, double));
  const char *ffspiropoint_to_string (ffspiropoint_t);

  size_t ffspiropoint_ivect_length (ffspiropoint_ivect_t);
  ffspiropoint_t ffspiropoint_ivect_ref (ffspiropoint_ivect_t, size_t);
  const ffspiropoint_t *ffspiropoint_ivect_ptr (ffspiropoint_ivect_t, size_t);
  const ffspiropoint_t *ffspiropoint_ivect_next (ffspiropoint_ivect_t, size_t, const ffspiropoint_t *);
  const ffspiropoint_t *ffspiropoint_ivect_prev (ffspiropoint_ivect_t, size_t, const ffspiropoint_t *);
  void ffspiropoint_ivect_refs (ffspiropoint_ivect_t, size_t i, size_t n, ffspiropoint_t array[]);
  ffspiropoint_ivect_t ffspiropoint_ivect_set (ffspiropoint_ivect_t, size_t, ffspiropoint_t);
  ffspiropoint_ivect_t ffspiropoint_ivect_sets (ffspiropoint_ivect_t, size_t i, size_t n, ffspiropoint_t array[]);
  ffspiropoint_ivect_t ffspiropoint_ivect_push (ffspiropoint_ivect_t, ffspiropoint_t);
  ffspiropoint_ivect_t ffspiropoint_ivect_pushes (ffspiropoint_ivect_t, size_t, ffspiropoint_t array[]);
  ffspiropoint_ivect_t ffspiropoint_ivect_pop (ffspiropoint_ivect_t);
  ffspiropoint_ivect_t ffspiropoint_ivect_pops (ffspiropoint_ivect_t, size_t);
  ffspiropoint_ivect_t ffspiropoint_ivect_slice (ffspiropoint_ivect_t, size_t start, size_t end);
  ffspiropoint_ivect_t ffspiropoint_ivect_append (ffspiropoint_ivect_t, ffspiropoint_ivect_t);

  # --------------------------------------------------------------

  ctypedef struct f64ivect_atomic_t__vector__:
    pass

  ctypedef const f64ivect_atomic_t__vector__ *ffspline_t

  ctypedef struct ffspline_ivect_t__vector__:
    pass

  ctypedef const ffspline_ivect_t__vector__ *ffspline_ivect_t

  size_t ffspline_length (ffspline_t);
  double ffspline_ref (ffspline_t, size_t);
  const double *ffspline_ptr (ffspline_t, size_t);
  const double *ffspline_next (ffspline_t, size_t, const double *);
  const double *ffspline_prev (ffspline_t, size_t, const double *);
  void ffspline_refs (ffspline_t, size_t i, size_t n, double array[]);
  ffspline_t ffspline_set (ffspline_t, size_t, double);
  ffspline_t ffspline_sets (ffspline_t, size_t i, size_t n, double array[]);
  ffspline_t ffspline_push (ffspline_t, double);
  ffspline_t ffspline_pushes (ffspline_t, size_t, double array[]);
  ffspline_t ffspline_pop (ffspline_t);
  ffspline_t ffspline_pops (ffspline_t, size_t);
  ffspline_t ffspline_slice (ffspline_t, size_t start, size_t end);
  ffspline_t ffspline_append (ffspline_t, ffspline_t);

  ffspline_t make_ffspline (size_t degree, double x[]);
  ffspline_t make_ffspline_linear (double x0, double x1);
  ffspline_t make_ffspline_quadratic (double x0, double x1, double x2);
  ffspline_t make_ffspline_cubic (double x0, double x1, double x2, double x3);

  void tensions_to_ffspline_cubic (_Bool perhaps_increase_tensions,
                                   double x0, double y0, double x3, double y3,
                                   double dir0x, double dir0y,
                                   double dir3x, double dir3y,
                                   double tension0, double tension3,
                                   ffspline_t *x, ffspline_t *y);
  void ffspline_cubic_tensions (ffspline_t x, ffspline_t y,
                                double *tension0, double *tension3);

  size_t ffspline_ivect_length (ffspline_ivect_t);
  ffspline_t ffspline_ivect_ref (ffspline_ivect_t, size_t);
  const ffspline_t *ffspline_ivect_ptr (ffspline_ivect_t, size_t);
  const ffspline_t *ffspline_ivect_next (ffspline_ivect_t, size_t, const ffspline_t *);
  const ffspline_t *ffspline_ivect_prev (ffspline_ivect_t, size_t, const ffspline_t *);
  void ffspline_ivect_refs (ffspline_ivect_t, size_t i, size_t n, ffspline_t array[]);
  ffspline_ivect_t ffspline_ivect_set (ffspline_ivect_t, size_t, ffspline_t);
  ffspline_ivect_t ffspline_ivect_sets (ffspline_ivect_t, size_t i, size_t n, ffspline_t array[]);
  ffspline_ivect_t ffspline_ivect_push (ffspline_ivect_t, ffspline_t);
  ffspline_ivect_t ffspline_ivect_pushes (ffspline_ivect_t, size_t, ffspline_t array[]);
  ffspline_ivect_t ffspline_ivect_pop (ffspline_ivect_t);
  ffspline_ivect_t ffspline_ivect_pops (ffspline_ivect_t, size_t);
  ffspline_ivect_t ffspline_ivect_slice (ffspline_ivect_t, size_t start, size_t end);
  ffspline_ivect_t ffspline_ivect_append (ffspline_ivect_t, ffspline_ivect_t);

  # --------------------------------------------------------------

  ctypedef struct ffcontour__:
    # We do not need to know the internals of struct ffcontour__; if
    # it turns out anyone needs them, they are doing this incorrectly.
    pass

  ctypedef const ffcontour__ *ffcontour_t

  ctypedef struct ffcontour_ivect_t__vector__:
    pass

  ctypedef const ffcontour_ivect_t__vector__ *ffcontour_ivect_t

  ctypedef struct ffcontour_bounding_box__:
    double x_min
    double y_min
    double x_max
    double y_max

  ctypedef const ffcontour_bounding_box__ *ffcontour_bounding_box_t

  ctypedef struct ffcontour_spline_after_point__:
    double x[4]
    double y[4]

  ctypedef const ffcontour_spline_after_point__ *ffcontour_spline_after_point_t

  ctypedef enum ffcontour_add_extrema_operation:
    FFCONTOUR_add_extrema_default = 255
    FFCONTOUR_add_all_extrema = 0
    FFCONTOUR_add_extrema_between_selected = 1
    FFCONTOUR_add_extrema_on_longer_splines = 2
    FFCONTOUR_add_extrema_and_merge_on_longer_splines = 3

  ctypedef enum ffcontour_simplification_operation:
    FFCONTOUR_do_cleanup = 0xFFFFFFFF
    FFCONTOUR_ignore_slopes = 0x01
    FFCONTOUR_allow_removal_of_extrema = 0x02
    FFCONTOUR_smooth_curves = 0x04
    FFCONTOUR_choose_hv = 0x08
    FFCONTOUR_force_lines = 0x10
    FFCONTOUR_snap_nearly_hv_lines = 0x20
    FFCONTOUR_merge_lines = 0x40
    FFCONTOUR_set_start_to_extremum = 0x80
    FFCONTOUR_remove_singleton_points = 0x100

  const char *ffcontour_to_string (ffcontour_t);

  ffcontour_t make_ffcontour (_Bool is_quadratic, _Bool is_closed,
                              const char *name);
  ffcontour_t ffcontour_clear (ffcontour_t);
  
  size_t ffcontour_length (ffcontour_t);
  size_t ffcontour_is_empty (ffcontour_t);
  
  size_t ffcontour_spiro_length (ffcontour_t);

  const char *ffcontour_name (ffcontour_t);
  ffcontour_t ffcontour_set_name (ffcontour_t, const char *);

  _Bool ffcontour_is_quadratic (ffcontour_t);
  _Bool ffcontour_is_cubic (ffcontour_t);
  void ffcontour_make_quadratic (ffcontour_t c, _Bool is_quadratic,
                                 ffcontour_t *result,
                                 const char **error_message);
  void ffcontour_make_cubic (ffcontour_t c, _Bool is_cubic,
                             ffcontour_t *result,
                             const char **error_message);

  _Bool ffcontour_is_closed (ffcontour_t);
  _Bool ffcontour_is_open (ffcontour_t);
  ffcontour_t ffcontour_make_closed (ffcontour_t c, _Bool is_closed);
  ffcontour_t ffcontour_make_open (ffcontour_t c, _Bool is_open);
  
  ffpoint_t ffcontour_ref (ffcontour_t, size_t);
  void ffcontour_refs (ffcontour_t, size_t, size_t n, ffpoint_t array[]);
  void ffcontour_slice (ffcontour_t, size_t start, size_t end);
  
  # Iteration over ffpoints.
  const ffpoint_t *ffcontour_ptr (ffcontour_t, size_t);
  const ffpoint_t *ffcontour_next (ffcontour_t, size_t, const ffpoint_t *);
  const ffpoint_t *ffcontour_prev (ffcontour_t, size_t, const ffpoint_t *);

  # Setting new values for ffpoints.
  ffcontour_t ffcontour_set (ffcontour_t, size_t, ffpoint_t);
  ffcontour_t ffcontour_sets (ffcontour_t, size_t i, size_t n,
                              ffpoint_t array[]);

  # Appending ffpoints.
  ffcontour_t ffcontour_push (ffcontour_t, ffpoint_t);
  ffcontour_t ffcontour_pushes (ffcontour_t, size_t n, ffpoint_t array[]);
  ffcontour_t ffcontour_append (ffcontour_t, ffcontour_t);

  # Removing ffpoints.
  ffcontour_t ffcontour_pop (ffcontour_t);
  ffcontour_t ffcontour_pops (ffcontour_t, size_t);

  ffcontour_t ffcontour_rotate_left (ffcontour_t, ssize_t);
  ffcontour_t ffcontour_rotate_right (ffcontour_t, ssize_t);

  ffcontour_t ffcontour_interpolate_on_curve_points (ffcontour_t);

  ssize_t ffcontour_find_next_on_curve (ffcontour_t, size_t);
  ssize_t ffcontour_find_prev_on_curve (ffcontour_t, size_t);
  ssize_t ffcontour_find_next_off_curve (ffcontour_t, size_t);
  ssize_t ffcontour_find_prev_off_curve (ffcontour_t, size_t);

  ffcontour_t ffcontour_start_drawing (ffcontour_t, double x0, double y0);
  ffcontour_t ffcontour_draw_line (ffcontour_t, ssize_t i,
                                   double x1, double y1);
  ffcontour_t ffcontour_draw_quadratic (ffcontour_t, ssize_t i,
                                        double x1, double y1,
                                        double x2, double y2);
  ffcontour_t ffcontour_draw_cubic (ffcontour_t,
                                    ssize_t i, double x1, double y1,
                                    double x2, double y2, double x3, double y3);

  ffcontour_t make_unit_shape_ffcontour (int n);

  ffcontour_t ffcontour_reverse (ffcontour_t);

  # The ‘the_contours_are_similar’ argument is int instead of bool
  # merely to make using compare_ffcontours in Cython a bit simpler,
  # by avoiding pointers to _Bool: the _Bool type is not built into
  # Cython currently (version 0.25.2).
  void ffcontour_similarity (ffcontour_t c1, ffcontour_t c2,
                             double point_err, double spline_err,
                             int *the_contours_are_similar,
                             const char **error_message1,
                             const char **error_message2);

  void ffcontour_x_bounds_at_y (ffcontour_t, double ybound1, double ybound2,
                                int *bounds_found, double *xmin, double *xmax,
                                const char **error_message);
  void ffcontour_y_bounds_at_x (ffcontour_t, double xbound1, double xbound2,
                                int *bounds_found, double *ymin, double *ymax,
                                const char **error_message);

  void ffcontour_is_self_intersecting (ffcontour_t c, int *is_self_intersecting,
                                       const char **error_message);

  void ffcontour_add_extrema (ffcontour_t c,
                              ffcontour_add_extrema_operation operation,
                              ssize_t emsize, ffcontour_t *result,
                              const char **error_message);
  void ffcontour_cluster (ffcontour_t c, double within, double max,
                          ffcontour_t *result, const char **error_message);
  ffcontour_t ffcontour_round (ffcontour_t c, double factor);

  ffcontour_t ffcontour_transform (ffcontour_t, ffpsmat_t);
  ffcontour_t ffcontour_transform_without_rounding (ffcontour_t, ffpsmat_t);
  void ffcontour_simplify (ffcontour_t c, double error_bound, int operations,
                           double tan_bounds, double linefixup, double linelenmax,
                           ffcontour_t *result, const char **error_message);

  ffcontour_bounding_box_t ffcontour_bounding_box (ffcontour_t);

  ffcontour_spline_after_point_t ffcontour_spline_after_ffpoint (ffcontour_t, size_t);

  void ffcontour_to_ffsplines (ffcontour_t, ffspline_ivect_t *x, ffspline_ivect_t *y,
                               const char **error_message);
  void ffsplines_to_ffcontour (ffspline_ivect_t x, ffspline_ivect_t y,
                               _Bool is_quadratic, _Bool is_closed, const char *name,
                               ffcontour_t *result, const char **error_message);

  void hobby_guide_to_ffcontour (size_t n_input,
                                 const double input_tokens[],
                                 const char *name,
                                 _Bool infinite_tensions_become_lines,
                                 ffcontour_t *result,
                                 int *info, const char **info_message,
                                 size_t *bad_token_index);
  void scheme_hobby_guide_to_ffcontour (const char *guide_string,
                                        const char *name,
                                        _Bool infinite_tensions_become_lines,
                                        ffcontour_t *result,
                                        const char **error_message);

  # Clockwise (1), counterclockwise (-1), or indeterminate (0)?
  int ffcontour_orientation (ffcontour_t);

  size_t ffcontour_ivect_length (ffcontour_ivect_t);
  ffcontour_t ffcontour_ivect_ref (ffcontour_ivect_t, size_t);
  const ffcontour_t *ffcontour_ivect_ptr (ffcontour_ivect_t, size_t);
  const ffcontour_t *ffcontour_ivect_next (ffcontour_ivect_t, size_t, const ffcontour_t *);
  const ffcontour_t *ffcontour_ivect_prev (ffcontour_ivect_t, size_t, const ffcontour_t *);
  void ffcontour_ivect_refs (ffcontour_ivect_t, size_t i, size_t n, ffcontour_t array[]);
  ffcontour_ivect_t ffcontour_ivect_set (ffcontour_ivect_t, size_t, ffcontour_t);
  ffcontour_ivect_t ffcontour_ivect_sets (ffcontour_ivect_t, size_t i, size_t n, ffcontour_t array[]);
  ffcontour_ivect_t ffcontour_ivect_push (ffcontour_ivect_t, ffcontour_t);
  ffcontour_ivect_t ffcontour_ivect_pushes (ffcontour_ivect_t, size_t, ffcontour_t array[]);
  ffcontour_ivect_t ffcontour_ivect_pop (ffcontour_ivect_t);
  ffcontour_ivect_t ffcontour_ivect_pops (ffcontour_ivect_t, size_t);
  ffcontour_ivect_t ffcontour_ivect_slice (ffcontour_ivect_t, size_t start, size_t end);
  ffcontour_ivect_t ffcontour_ivect_append (ffcontour_ivect_t, ffcontour_ivect_t);

  # --------------------------------------------------------------

  ctypedef struct fflayer__:
    # We do not need to know the internals of struct fflayer__; if
    # it turns out anyone needs them, they are doing this incorrectly.
    pass

  ctypedef const fflayer__ *fflayer_t

  fflayer_t make_fflayer (_Bool is_quadratic, ffcontour_ivect_t contours);

  size_t fflayer_length (fflayer_t);
  ffcontour_t fflayer_ref (fflayer_t, size_t);
  const ffcontour_t *fflayer_ptr (fflayer_t, size_t);
  const ffcontour_t *fflayer_next (fflayer_t, size_t, const ffcontour_t *);
  const ffcontour_t *fflayer_prev (fflayer_t, size_t, const ffcontour_t *);
  void fflayer_refs (fflayer_t, size_t i, size_t n, ffcontour_t array[]);
  fflayer_t fflayer_set (fflayer_t, size_t, ffcontour_t);
  fflayer_t fflayer_sets (fflayer_t, size_t i, size_t n, ffcontour_t array[]);
  fflayer_t fflayer_push (fflayer_t, ffcontour_t);
  fflayer_t fflayer_pushes (fflayer_t, size_t, ffcontour_t array[]);
  fflayer_t fflayer_pop (fflayer_t);
  fflayer_t fflayer_pops (fflayer_t, size_t);
  fflayer_t fflayer_slice (fflayer_t, size_t start, size_t end);
  fflayer_t fflayer_append (fflayer_t, fflayer_t);

  _Bool fflayer_is_empty (fflayer_t);
  _Bool fflayer_is_quadratic (fflayer_t);
  _Bool fflayer_is_cubic (fflayer_t);
  void fflayer_make_quadratic (fflayer_t y, _Bool is_quad,
                               fflayer_t *result, const char **error_message);
  void fflayer_make_cubic (fflayer_t y, _Bool is_cubic,
                           fflayer_t *result, const char **error_message);

  fflayer_t fflayer_reverse (fflayer_t);

  void fflayer_add_extrema (fflayer_t c,
                            ffcontour_add_extrema_operation operation,
                            size_t emsize, fflayer_t *result,
                            const char **error_message);
  void fflayer_cluster (fflayer_t c, double within, double max,
                        fflayer_t *result, const char **error_message);
  fflayer_t fflayer_round (fflayer_t c, double factor);
  void fflayer_simplify (fflayer_t c, double error_bound, int operations,
                         double tan_bounds, double linefixup, double linelenmax,
                         fflayer_t *result, const char **error_message);

  void fflayer_similarity (fflayer_t y1, fflayer_t y2,
                           double point_err, double spline_err,
                           int *the_layers_are_similar,
                           const char **error_message1,
                           const char **error_message2);

  # --------------------------------------------------------------
